<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_article extends Model
{
    public function article(){
        if($this->hasOne('App\Post','posts_id','articleID')->where('langID',session('languageID'))->exists()){
            return $this->hasOne('App\Post','posts_id','articleID')->where('langID',session('languageID'));
        }
        return $this->hasOne('App\Post','posts_id','articleID')->where('langID',1);
    }

    public function posts(){
        if($this->hasOne('App\Post','posts_id','articleID')->where('langID',session('languageID'))->exists()){
            return $this->hasOne('App\Post','posts_id','articleID')->where('langID',session('languageID'));
        }
        return $this->hasOne('App\Post','posts_id','articleID')->where('langID',1);
    }

}
