<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_texts_image extends Model
{
    public function image_data(){
        return $this->belongsTo('App\Image','imageID','id')->orderBy('order_id','asc');
    }
}
