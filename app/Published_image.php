<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Published_image extends Model
{
    public function image(){
        return $this->belongsTo('App\Image','imageID','id')->orderBy('order_id','asc');
    }

}
