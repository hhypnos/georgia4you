<?php
/**
 * Created by PhpStorm.
 * User: hypnos
 * Date: 7/24/19
 * Time: 2:48 AM
 */

namespace App;


class Elastic
{
    private $hosts = [];
    private $client = '';

    public function __construct(){
        $this->hosts = [
            '81.16.248.238:8097'         // IP + Port
        ];
        $this->client = \Elasticsearch\ClientBuilder::create()->setHosts($this->hosts)->build();
    }

    /**
     * Store multiple data
     *
     * @param  string $index
     * @param  string $type
     * @param  array $data_array
     * @return $response
     */
    public function storeData(string $index,string $type, array $data_array){

            $data['body'][] = [
                'index' => [
                    '_index'    => $index,
                    '_type'     => $type
                ]
            ];

            $data['body'][] = $data_array;

            // Store data
            $response = $this->client->bulk($data);

            return $response;
    }

    /**
     * Store single data
     *
     * @param  string $index
     * @param  string $type
     * @param  array $data_array
     * @return $response
     */
    public function storeSingleData(string $index,string $type, array $data_array){
        $data = [
            'body' => $data_array,
            'index' => $index,
            'type'  => $type,
        ];

        // Store single data
        $response = $this->client->index($data);

        return $response;
    }

    /**
     * Search data
     * @param  integer $size
     * @param  string $index
     * @param string $type
     * @param array $data_sort
     * @param array $data_array
     * @return $response
     */
    function searchData(int $size, string $index, string $type, array $data_sort = ['_score'], array $data_query){
        $params = [
            'size'=>$size,
            'index' => $index,
            'type' => $type,
            'body' => [
                'sort' => $data_sort,
                'query' => $data_query,
            ]
        ];

        // Fetch Data

        if(! $this->client->indices()->exists(['index' => $index])) {
            abort(404);
        }

        $response = $this->client->search($params);

        return $response;
    }

    /**
     * Delete single data
     *
     * @param  string $index
     * @param string $type
     * @param string $id
     * @return $response
     */
    function deleteSingleData(string $index, string $type, string $id){
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id
        ];

        // Fetch Data
        $response = $this->client->delete($params);

        return $response;
    }

    /**
     * Delete Index
     *
     * @param  string $name
     * @return $response
     */
    public function deleteDataByIndex($name)
    {
        if (! $this->client->indices()->exists(['index' => $name])) {
            abort(404);
        }
         $response = $this->client->indices()->delete([
            'index' => $name
        ]);
        return $response;
    }

    /**
     * Delete data by query
     *
     *  @param  int $size
     * @param  string $index
     * @param string $type
     * @param array $data_sort
     * @param array $data_array
     * @return $response
     */
    public function deleteDataByQuery(int $size,string $index, string $type,array $data_sort = ['_score'], array $data_query){
        $params = [
            'size'  => $size,
            'index' => $index,
            'type'  => $type,
            'body'  => [
                'sort' => $data_sort,
                'query' => $data_query,
            ]
        ];

        // Delete Data
        if (! $this->client->indices()->exists(['index' => $index])) {
            abort(404);
        }

        $response = $this->client->deleteByQuery($params);

        return $response;
    }

    /**
     * get client
     *
     */
    public function getClient()
    {
        return $this->client;
    }
}
