<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Published_text extends Model
{
    public function text(){
        return $this->belongsTo('App\Post_text','post_textID','post_texts_id')->where('langID',session('languageID'));
    }
}
