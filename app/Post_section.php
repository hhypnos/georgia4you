<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_section extends Model
{
    public function posts(){
        if($this->hasOne('App\Post','posts_id','postID')->where('langID',session('languageID'))->exists()){
            return $this->hasOne('App\Post','posts_id','postID')->where('langID',session('languageID'));
        }
        return $this->hasOne('App\Post','posts_id','postID')->where('langID',1);
    }
}
