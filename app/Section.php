<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function post_sections(){
        return $this->hasMany('App\Post_section','section_id','section_id')->orderBy('order_id','asc');
    }

    public function section_styles(){
        return $this->hasOne('App\Section_style','section_style_id','section_style_id');
    }
}
