<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_inner_slider extends Model
{
    public function fetch_image(){
        return $this->belongsTo('App\Image','imageID','id');
    }
}
