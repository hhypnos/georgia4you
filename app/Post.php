<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function cover_image(){
        return $this->hasOne('App\Published_image','contentID','posts_id')->where('image_genderID',1)->where('defaultOrNot','2')->orderBy('id','desc');
    }

    public function all_images(){
        return $this->hasMany('App\Published_image','contentID','posts_id')->where('image_genderID',1)->orderBy('order_id','asc');
    }

    public function slider(){
        return $this->hasMany('App\Post_inner_slider','postID','posts_id')->where('enable_disable',2)->orderBy('order_id','asc');
    }

    public function post_texts(){
        return $this->hasMany('App\Published_text','contentID','posts_id')->orderBy('id','asc');
    }

    public function inSections(){
        return $this->hasMany('App\Post_section','postID','posts_id');
    }
    //check if post is in production...(has section)...
    public function scopeDraftNotIn($query,$select,$from){
       return $query->whereNotIn('posts_id',function($query) use ($select,$from) {
            $query->select($select)->from($from);
        });
    }

    public function post_articles(){
        return $this->hasMany('App\Post_article','postID','posts_id');
    }

    public function scopeDraftIn($query,$select,$from){
        return $query->whereIn('posts_id',function($query) use ($select,$from) {
            $query->select($select)->from($from);
        });
    }
}
