<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_text extends Model
{
    public function text_images(){
        return $this->hasMany('App\Post_texts_image','contentID','post_texts_id')->orderBy('order_id','asc');
    }
}
