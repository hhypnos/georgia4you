<?php

namespace App\Http\Controllers;

use App\Post_section;
use App\Post_text;
use App\Post_texts_image;
use App\Published_text;
use http\Env\Response;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    private $language;
    public function __construct()
    {
        if(session('languageID') === null){
            session(['languageID'=>1]);
        }
        $this->language = session('languageID');
    }
    public function index()
    {
    return view('dashboard.index')->with(['title'=>'A Panel']);
    }
    //show all posts with sections & articles in it...
    public function section_content()
    {
        $sections = \App\Section::orderBy('order_by','asc')->get();
        $section_styles = \App\Section_style::get();
        return view('dashboard.section_content',compact('sections','section_styles'))->with(['title'=>'A Panel']);
    }

    public function section_apply_style()
    {
        $this->validate(request(),
            [
                'sectionstyleID' =>'required|exists:section_styles,section_style_id',
                'sectionID' => 'required'
            ]
        );

        $section = \App\Section::where('section_id',request('sectionID'))->firstOrFail();
        $section->section_style_id = request('sectionstyleID');
        $section->save();

        return response()->json(['info'=>'style changed succussfully',]);

    }

    public function article_content()
    {
        $posts = \App\Post::orderBy('created_at','asc')->get();
        return view('dashboard.article_content',compact('posts'))->with(['title'=>'A Panel']);
    }
    public function changeorder()
    {
        $this->validate(request(),
            [
                'postID' =>'required',
                'sectionID' => 'required',
                'orderID'=>'required'
            ]
        );
        $post_section = \App\Post_section::where('postID',request('postID'))->where('section_id',request('sectionID'))->firstOrFail();
        $post_section->order_id = request('orderID');
        $post_section->save();
        return response()->json(['info'=>'post deleted succussfully','sectionID'=>request('sectionID'),'postID'=>request('postID')]);
    }

    public function changeorder_article()
    {
        $this->validate(request(),
            [
                'postID' =>'required',
                'articleID' => 'required',
                'orderID'=>'required'
            ]
        );
        $post_section = \App\Post_article::where('postID',request('postID'))->where('articleID',request('articleID'))->firstOrFail();
        $post_section->order_id = request('orderID');
        $post_section->save();
        return response()->json(['info'=>'post deleted succussfully','sectionID'=>request('sectionID'),'postID'=>request('postID')]);
    }

    public function delete_post_from_section()
    {
        $this->validate(request(),
            [
               'postID' =>'required',
                'sectionID' => 'required'
            ]
        );
        $post_section = \App\Post_section::where('postID',request('postID'))->where('section_id',request('sectionID'))->firstOrFail();
        $post_section->delete();
        return response()->json(['info'=>'post deleted succussfully']);
    }

    public function delete_article_from_post()
    {
        $this->validate(request(),
            [
                'postID' =>'required',
                'articleID' => 'required'
            ]
        );
        $post_section = \App\Post_article::where('postID',request('postID'))->where('articleID',request('articleID'))->firstOrFail();
        $post_section->delete();
        return response()->json(['info'=>'post deleted succussfully']);
    }

    public function drafts()
    {
        $posts = \App\Post::draftNotIn('postID','post_sections')->draftNotIn('articleID','post_articles')->get();
        $sections_posts = \App\Post::draftIn('postID','post_sections')->get();
        $sections = \App\Section::orderBy('order_by','asc')->get();
        return view('dashboard.draft',compact('posts','sections','sections_posts'))->with(['title'=>'A Panel']);
    }

    public function preview_draft($slug)
    {
        $post = \App\Post::where('slug',$slug)->where('langID',session('languageID'))->first();
        if(empty($post)){
            $post = \App\Post::where('slug',$slug)->where('langID',1)->firstOrFail();
        }

//For preview purpose
        $post_section = Post_section::where('section_id',1)->where('postID',1)->first();
        $post_section_prev = Post_section::where('section_id',$post_section->section_id)->where('order_id',$post_section->order_id - 1)->first();
        $post_section_next = Post_section::where('section_id',$post_section->section_id)->where('order_id',$post_section->order_id + 1)->first();
        $full_section = Post_section::where('section_id',1)->get();
        $post_section_first = Post_section::where('section_id',1)->where('order_id',$full_section->min('order_id'))->first();
        $post_section_last = Post_section::where('section_id',1)->where('order_id',$full_section->max('order_id'))->first();

        return view('post.post',compact('post'))->with([
            "title"=>$post->title,
            "description"=>$post->metaDescription,
            "post_section"=>$post_section,
            "post_section_prev"=>$post_section_prev,
            "post_section_next"=>$post_section_next,
            "full_section"=>$full_section,
            "post_section_first"=>$post_section_first,
            "post_section_last"=>$post_section_last,

        ]);
    }

    public function new_post()
    {
        $sections = \App\Section::orderBy('order_by','asc')->get();
        return view('dashboard.newpost',compact('sections'))->with(["title"=>'Insert New post',"description"=>'New Description']);
    }

    public function insert_post()
    {
        $this->validate(request(),
            [
                'title' => 'required',
                'subtitle'=> 'required',
                'description'=> 'required',
                'metaDescription'=> 'required',
                'slug' => 'required|unique:posts,slug',
                'image_genderID' =>'required',
                'upl.*' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:5000'
            ]
        );
        //START: Custom errors if laravel validation fails
        if(\App\Post::where('slug',str_replace(" ","-",strtolower(trim(request('slug')))))->exists()){
            session(['errorType'=>'warning']);
            return back()->withErrors(['errors'=>'slug already exists'])->withInput(request()->input());
        }
        if(empty(request('upl'))){
            session(['errorType'=>'info']);
            return back()->withErrors(['errors'=>'You must upload at least 1 image'])->withInput(request()->input());
        }
        //END: Custom errors if laravel validation fails
        $post = new \App\Post;
        $post->title = trim(request('title'));
        $post->subtitle = trim(request('subtitle'));
        $post->description = request('description');
        $post->metaDescription = request('metaDescription');
        $post->text = request('text');
        $post->slug = str_replace(" ","-",strtolower(trim(request('slug'))));
        $post->langID =  1;
        $post->save();
        $lastInsertedPostID = $post->id;//make last inserted postID static...
// IMAGE CONTROL
        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $key => $image) {
                $name = 'assets/images/upl/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image;
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $published_image = new \App\Published_image;
                //if first image then make it default;
                if($key === 0){
                    $published_image->defaultOrNot = 2;
                }
                $published_image->imageID = $photo->id;
                $published_image->contentID = $post->id;
                $published_image->order_id = $key+1;//order by first to last...
                $published_image->image_genderID = request('image_genderID');
                $published_image->save();
            }
        }

// END IMAGE CONTROL

// SECTION CONTROL
        if(!empty(request('sections'))) {
            $sections = \App\Section::pluck('section_id')->toArray();
            foreach($sections as $section) {
                if (in_array($section,request('sections'))) {
                    $post = \App\Post_section::where('postID', $lastInsertedPostID)->where('section_id', $section)->first();
                    if (empty($post)) {
                        $post_section = new \App\Post_section;
                        $post_section->section_id = $section;
                        $post_section->postID = $lastInsertedPostID;
                        $post_section->order_id = 99;
                        $post_section->save();
                    }
                }else{
                    $post = \App\Post_section::where('postID', $lastInsertedPostID)->where('section_id', $section)->first();
                    if (!empty($post)) {
                        $post->delete();
                    }
                }
            }
        }
//END SECTION CONTROL
        //add posts_id
        $post_update_id = \App\Post::find($lastInsertedPostID);
        $post_update_id->posts_id = $lastInsertedPostID;
        $post_update_id->save();
        return response()->json(['info'=>'post created succussfully']);
    }

    public function insert_post_text()
    {
        $this->validate(request(),[
            'post_id'=>'required|exists:posts,id',
            'upl.*' => 'image|mimes:jpeg,jpg,png,gif,bmp|max:5000',
            'title'=>'',
            'postText'=>'required',
            'leftOrRight'=>'required|in:left,right'
        ]);

        $post_text = new Post_text();
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = request('leftOrRight');
        $post_text->save();

        $published_text = new Published_text();
        $published_text->post_textID = $post_text->id;
        $published_text->contentID = request('post_id');
        $published_text->save();

        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/upl/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image();
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $post_texts_image = new \App\Post_texts_image();
                $post_texts_image->imageID = $photo->id;
                $post_texts_image->contentID = $post_text->id;
                $post_texts_image->image_genderID = request('image_genderID');
                $post_texts_image->save();
            }
            return response()->json(['status' => 'success']);
        }


        //add posts texts id
        $post_texts_update_id = \App\Post_text::find($post_text->id);
        $post_texts_update_id->post_texts_id = $post_text->id;
        $post_texts_update_id->save();

        return response()->json(['info'=>'Post text succussfully inserted ']);
    }

    public function delete_post_text()
    {
        $this->validate(request(),[
            'post_text_id'=>'required|exists:published_texts,post_textID',
        ]);

        $published_text = Published_text::where('post_textID',request('post_text_id'))->firstOrFail();
        $published_text->delete();

        return response()->json(['info'=>'Post text succussfully Removed ']);
    }

    public function delete_post_text_image()
    {
        $this->validate(request(),[
            'imageID'=>'required|exists:post_texts_images,id',
        ]);

        $published_text_image = Post_texts_image::findOrFail(request('imageID'));
        $published_text_image->delete();

        return response()->json(['info'=>'Post text image succussfully Removed ']);
    }

    public function update_post_text()
    {
        $this->validate(request(),[
            'post_text_id'=>'required|exists:published_texts,post_textID',
            'upl.*' => 'image|mimes:jpeg,jpg,png,gif,bmp|max:5000',
            'title'=>'required',
            'postText'=>'required',
            'leftOrRight'=>'required|in:left,right'
        ]);

        $post_text = Post_text::find(request('post_text_id'));
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = request('leftOrRight');
        $post_text->breadcrumbs = request('breadcrumbs') !== null ? request('breadcrumbs') : 1 ;
        $post_text->save();

        $published_text = Published_text::where('post_textID',request('post_text_id'))->first();
        $published_text->order_id = request('order_id');
        $published_text->save();

        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/upl/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image();
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $post_texts_image = new \App\Post_texts_image();
                $post_texts_image->imageID = $photo->id;
                $post_texts_image->contentID = $post_text->id;
                $post_texts_image->image_genderID = request('image_genderID');
                $post_texts_image->save();
            }
            return response()->json(['status' => 'success']);
        }

        return response()->json(['info'=>'Post text succussfully inserted ']);
    }


    //Post inner slider
    public function upload_slider_image()
    {
        // A list of permitted file extensions
        $this->validate(request(),[
            'post_id' =>'required',
            'upl.*' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:5000'
        ]);

        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/post_inner_slider/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/post_inner_slider/', $name);
                $photo = new \App\Image;
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $post_inner_slider = new \App\Post_inner_slider();
                $post_inner_slider->imageID = $photo->id;
                $post_inner_slider->postID = request('post_id');
                $post_inner_slider->save();
            }
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);

    }

    public function order_slider_image()
    {
        $this->validate(request(),
            [
                'imageID' =>'required',
                'orderID'=>'required'
            ]
        );
        $image = \App\Post_inner_slider::where('imageID',request('imageID'))->firstOrFail();
        $image->order_id = request('orderID');
        $image->save();
        return response()->json(['info'=>'Order Changed']);
    }

    public function delete_slider_image()
    {
        $this->validate(request(),
            [
                'imageID' =>'required',
                'contentID'=>'required'
            ]
        );
        $image = \App\Post_inner_slider::where('imageID',request('imageID'))->where('postID',request('contentID'))->firstOrFail();
        $image->delete();
        return response()->json(['info'=>'Image Deleted Succussfully']);
    }

    public function publishpost()
    {
        $this->validate(request(),
            [
                'postID' =>'required',
                'sectionID' => 'required',
                'orderID'=>'required'
            ]
        );
        $post_section = new \App\Post_section;
        $post_section->section_id = request('sectionID');
        $post_section->postID = request('postID');
        $post_section->order_id = request('orderID');
        $post_section->save();
        return response()->json(['info'=>'draft published succussfully']);
    }

    public function publish_article()
    {
        $this->validate(request(),
            [
                'postID' =>'required',
                'articleID' => 'required',
                'orderID'=>'required'
            ]
        );
        $post_article = new \App\Post_article;
        $post_article->articleID = request('articleID');
        $post_article->postID = request('postID');
        $post_article->order_id = request('orderID');
        $post_article->save();
        return response()->json(['info'=>'draft published succussfully']);
    }

    public function deletepostfromdrafts()
    {
        $this->validate(request(),
            [
                'postID' =>'required'
            ]
        );

        $draft = \App\Post::where('id',request('postID'))->firstOrFail();
        $draft->delete();
        return response()->json(['info'=>'draft deleted succussfully']);
    }

    public function post($slug){
        if(empty($post)){
            $post = \App\Post::where('slug',$slug)->where('langID',1)->firstOrFail();
            $sections = \App\Section::orderBy('order_by','asc')->get();
        }
        return view('dashboard.post',compact('post','sections'))->with(["title"=>$post->title,"description"=>$post->metaDescription]);
    }

    public function translates($post_id,$langID){
            $post = \App\Post::where('posts_id',$post_id)->where('langID',$langID)->first();
            $post_original = \App\Post::where('posts_id',$post_id)->where('langID',1)->first();
        return view('dashboard.translates',compact('post','post_original'))->with(["title"=>'translate',"description"=>'translate']);
    }

    public function text_translates($text_id,$langID){
        $post = \App\Post_text::where('post_texts_id',$text_id)->where('langID',$langID)->first();
        $post_original = \App\Post_text::where('post_texts_id',$text_id)->where('langID',1)->first();
        return view('dashboard.text_translates',compact('post','post_original'))->with(["title"=>'translate',"description"=>'translate']);
    }

    public function update_translates($post_id,$langID){

        $this->validate(request(),
            [
                'title' => 'required',
                'subtitle'=> 'required',
                'description'=> 'required',
                'metaDescription'=> 'required',
            ]
        );

        $post_original = \App\Post::where('posts_id',$post_id)->where('langID',1)->first();

        $post = \App\Post::where('posts_id',$post_id)->where('langID',$langID)->first();
        if(empty($post)) {
            $post = new \App\Post;
        }
        $post->posts_id = $post_id;
        $post->title = trim(request('title'));
        $post->subtitle = trim(request('subtitle'));
        $post->description = request('description');
        $post->metaDescription = request('metaDescription');
        $post->text = request('text');
        $post->langID =  $langID;
        $post->slug =  $post_original->slug;
        $post->save();

        return response()->json(['info'=>'translated']);
    }

    public function update_text_translates($text_id,$langID){

        $this->validate(request(),[
            'post_texts_id'=>'required|exists:post_texts,post_texts_id',
            'title'=>'required',
            'postText'=>'required',
        ]);

        $post_original = \App\Post_text::where('post_texts_id',$text_id)->where('langID',1)->first();
        $post_text = \App\Post_text::where('post_texts_id',$text_id)->where('langID',$langID)->first();
        if(empty($post)) {
            $post_text = new \App\Post_text;
        }
        $post_text->post_texts_id = $text_id;
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = $post_original->leftOrRight;
        $post_text->langID =  $langID;
        $post_text->save();

        return response()->json(['info'=>'translated']);
    }



      /*
       * Delete Post
       */
    public function delete_post(){
        $this->validate(request(),
            [
                'post_id' =>'required',
            ]
        );
        $post_sections = \App\Post_section::where('postID', request('post_id'))->get();

        foreach($post_sections as $post_section){
            $post_section->delete();
        }

        $images = \App\Image::where('contentID',request('contentID'))->get();

        foreach ($images as $image){
                $image->delete();
        }

        $post = \App\Post::where('id',request('post_id'))->where('langID',1)->firstOrFail();
        $post->delete();
        return response()->json(['info'=>'Post Deleted']);
    }

       /*
        * Update Post
        */
    public function update_post(){
        $this->validate(request(),
            [
                'post_id' =>'required',
                'title' =>'required|max:191',
                'subtitle' =>'required|max:191',
                'description' =>'required',
                'metaDescription' =>'required|max:191',
                'postText'=>'required',
                'slug' =>'required|max:191',
            ]
        );

//        SECTION CONTROL
        $sections = \App\Section::pluck('section_id')->toArray();
        foreach($sections as $section) {
            if (in_array($section,request('sections'))) {
                $post = \App\Post_section::where('postID', request('post_id'))->where('section_id', $section)->first();
                if (empty($post)) {
                    $post_section = new \App\Post_section;
                    $post_section->section_id = $section;
                    $post_section->postID = request('post_id');
                    $post_section->order_id = 99;
                    $post_section->save();
                }
            }else{
                $post = \App\Post_section::where('postID', request('post_id'))->where('section_id', $section)->first();
                if (!empty($post)) {
                    $post->delete();
                }
            }
        }
//        END SECTION CONTROL
            $post = \App\Post::where('id',request('post_id'))->where('langID',1)->firstOrFail();
            $post->title = request('title');
            $post->subtitle = request('subtitle');
            $post->description = request('description');
            $post->metaDescription = request('metaDescription');
            $post->text = request('postText');
            $post->slug = request('slug');
            $post->save();
            return response()->json(['info'=>'Post Updated']);
    }
    /*
     * Post's IMAGE manipulation
     */
    public function upload_image(){
        // A list of permitted file extensions
        $this->validate(request(),[
            'post_id' =>'required',
            'image_genderID' =>'required',
            'upl.*' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:5000'
        ]);

        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/upl/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image;
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $published_image = new \App\Published_image;
                $published_image->imageID = $photo->id;
                $published_image->contentID = request('post_id');
                $published_image->image_genderID = request('image_genderID');
                $published_image->save();
            }
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

    public function tiny_photo_upload()
    {
        $image = request()->file('file');
        $name = \URL::to('assets/images/upl/'.time().'-'.$image->getClientOriginalName());
        $image->move('assets/images/upl/', $name);

        return  json_encode(array('location' => $name));
    }

    public function change_image_order(){
        $this->validate(request(),
            [
                'imageID' =>'required',
                'orderID'=>'required'
            ]
        );
        $image = \App\Published_image::where('imageID',request('imageID'))->firstOrFail();
        $image->order_id = request('orderID');
        $image->save();
        return response()->json(['info'=>'Order Changed']);
    }

    public function make_default_image(){
        $this->validate(request(),
            [
                'imageID' =>'required',
                'contentID'=>'required'
            ]
        );
        $images = \App\Published_image::where('contentID',request('contentID'))->get();
        foreach ($images as $image){
            if($image->defaultOrNot == 2){
                $image->defaultOrNot=1;
                $image->save();
            }
        }
        $image = \App\Published_image::where('imageID',request('imageID'))->where('contentID',request('contentID'))->firstOrFail();
        $image->defaultOrNot = 2;
        $image->save();
        return response()->json(['info'=>'Image set to default']);
    }

    public function delete_image(){
        $this->validate(request(),
            [
                'imageID' =>'required',
                'contentID'=>'required'
            ]
        );
        $image = \App\Published_image::where('imageID',request('imageID'))->where('contentID',request('contentID'))->firstOrFail();
            if($image->defaultOrNot == 2){
                $changeDefault = \App\Published_image::where('imageID','!=',request('imageID'))->where('contentID',request('contentID'))->firstOrFail();
                $changeDefault->defaultOrNot = 2;
                $changeDefault->save();
            }
        $image->delete();
        return response()->json(['info'=>'Image Deleted Succussfully']);
    }
    /*
    *  Cover Image
    */
    public function cover_images(){
        $cover_images = \App\Cover_image::get();
        return view('dashboard.cover_image',compact('cover_images'))->with(["title"=>"Cover Images","description"=>""]);
    }


    public function delete_cover_images(){
        $this->validate(request(),
            [
                'image_id' =>'required',
            ]
        );
        $image = \App\Cover_image::where('image_id',request('image_id'))->firstOrFail();
        $image->delete();
        return response()->json(['info'=>'Cover Image Deleted Succussfully']);
    }

    public function upload_cover_images(){
        // A list of permitted file extensions
        $this->validate(request(),[
            'upl.*' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:55000'
        ]);

        if(request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/cover_images/'.time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/cover_images/', $name);
                $photo = new \App\Cover_image;
                $photo->image = $name;
                $photo->langID = 1;
                $photo->save();
                $update_id = \App\Cover_image::find($photo->id);
                $update_id->image_id = $photo->id;
                $update_id->save();
            }
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }
    /*
     *  AUTHENTIFICATION
     */
    public function signin()
    {
        $credentialsWithEmail = array('email' => request('email'), 'password'=>request('password'));

        $rememberMe = false;
//        if(request()->get('rememberme') != null){
//            $rememberMe = true;
//        }
        $user = User::where('email',request('email'))->firstOrFail();
        if(!auth()->attempt($credentialsWithEmail,$rememberMe) && $user->activation>=3){
            return 'error';
        }

        return redirect('account');
    }

    public function logout()
    {

        auth()->logout();

        return redirect('login');
    }
}
