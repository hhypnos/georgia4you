<?php

namespace App\Http\Controllers;

use App\Post_article;
use App\Post_inner_slider;
use Illuminate\Http\Request;
use App\Language;
use App\Currency;
use App\Static_word;
use App\Section;
use App\Post;
use App\Post_section;

class MainController extends Controller
{
    private $language;
    public function __construct()
    {
        if(session('languageID') === null){
            session(['languageID'=>1]);
        }
        $this->language = session('languageID');
    }

    /**
     *
     * Main Controller
     *
     **/
    public function index(){
        $sections = Section::orderBy('order_by','asc')->get();

        return view(mobile_view('mainpage.welcome','mainpage.mobile'),compact('sections','post_sections'))->with([
            "title"=>translate('GEORGIA 4 YOU (CAUCASUS) | Best Travel Advises and Highlights of Georgia',session('languageID')),
            "description"=>translate('Info site for travelers to Georgia, Caucasus. Useful info, activities, events, highlights, places to visit. Plan your road trip with ready routes and maps for drivers.',session('languageID'))
        ]);
    }

    public function post($section_name,$slug, $article_slug = null){
        $sectiontype = Section::where('section_slug',$section_name)->firstOrFail();


        $post = Post::DraftIn('postID','post_sections')->where('slug',$slug)->where('langID',session('languageID'))->first();
//        dd($post);
        if(empty($post)){
            $post = Post::DraftIn('postID','post_sections')->where('slug',$slug)->where('langID',1)->firstOrFail();
        }

        $post_section = Post_section::where('section_id',$sectiontype->section_id)->where('postID',$post->posts_id)->firstOrFail();

        $post_section_prev = Post_section::where('section_id',$post_section->section_id)->where('order_id',$post_section->order_id - 1)->first();
        $post_section_next = Post_section::where('section_id',$post_section->section_id)->where('order_id',$post_section->order_id + 1)->first();

        $full_section = Post_section::where('section_id',$sectiontype->section_id)->get();
        $post_section_first = Post_section::where('section_id',$post_section->section_id)->where('order_id',$full_section->min('order_id'))->first();
        $post_section_last = Post_section::where('section_id',$post_section->section_id)->where('order_id',$full_section->max('order_id'))->first();

        if($sectiontype->section_id !== $post_section->section_id ){
            abort(404);
        }

        //fetch articles by post
        if(!is_null($article_slug)) {
            $post = Post::DraftIn('articleID', 'post_articles')->where('slug', $article_slug)->where('langID', session('languageID'))->first();
            if (empty($post)) {
                $post = Post::DraftIn('articleID', 'post_articles')->where('slug', $article_slug)->where('langID', 1)->firstOrFail();
            }

            $parent_post = Post::DraftIn('postID','post_sections')->where('slug',$slug)->where('langID',session('languageID'))->first();

            $post_section = Post_article::where('postID',$parent_post->posts_id)->where('articleID',$post->posts_id)->firstOrFail();

            $post_section_prev = Post_article::where('postID',$post_section->postID)->where('order_id',$post_section->order_id - 1)->first();
            $post_section_next = Post_article::where('postID',$post_section->postID)->where('order_id',$post_section->order_id + 1)->first();

            $full_section = Post_article::where('postID',$post_section->postID)->get();
            $post_section_first = Post_article::where('postID',$post_section->postID)->where('order_id',$full_section->min('order_id'))->first();
            $post_section_last = Post_article::where('postID',$post_section->postID)->where('order_id',$full_section->max('order_id'))->first();
        }

        return view('post.post',compact('post','sectiontype','post_section_prev','post_section_next','post_section_first','post_section_last'))->with(["title"=>$post->title,"description"=>$post->metaDescription]);
    }


    /**
     * Send feedback
     **/
    public function send_feedback()
    {
        $this->validate(request(),[
            'email'=>'required|email',
            'name'=>'required|max:30',
            'subject'=>'required',
            'text'=>'required'
        ]);
        $emailOptions = array(
            'email'=>request('email'),
            'name'=>request('name'),
            'subject'=>request('subject'),
            'text' => request('text')
        );
        \Mail::send([], $emailOptions, function($message){
            $message->to(env('MAIL_USERNAME'))->subject('Georgia4you Feedback: '.request('subject'));
            $message->from(request('email') ,request('email'));
            $message->setBody(request('text') );
        });

        return response()->JSON(['info'=>'Email Sent, thanks for your email']);
    }


    /**
     * @param $lang
     * @return translated text
     **/
    public function language($lang)
    {
        $language = Language::where('langID',$lang)->first();

        if($language){
            session(['languageID' => $language->langID]);
            if (url()->previous() != url()->current() && strpos(url()->previous(), env('APP_URL')) !== false) {
                return redirect(url()->previous());

            }
            return redirect(url()->to('/'));
        }
        return redirect('/')->withErrors(['error'=>translate('language not found',session('languageID'))]);
    }

    /**
     * @param $currencyID
     * @return currency of day
     */
    public function currency($currencyID)
    {
        $currency = Currency::where('id',$currencyID)->first();
        if($currency){
            session([
                "currency" => array(
                    "currency"=>$currency->currency,
                    "logo"=>$currency->logo,
                    "value"=>$currency->value
                )
            ]);
            return back();
        }
        return redirect('/')->withErrors(['error'=>translate('Currency not found',session('languageID'))]);
    }
//END: Currency
}
