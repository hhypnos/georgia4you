<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- Bootstrap -->
<script src="{{URL::to('assets/dashboard_assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{URL::to('assets/dashboard_assets/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{URL::to('assets/dashboard_assets/vendors/nprogress/nprogress.js')}}"></script>
<!-- jQuery custom content scroller -->
<script src="{{URL::to('assets/dashboard_assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- iCheck -->
<script src="{{URL::to('assets/dashboard_assets/vendors/iCheck/icheck.min.js')}}"></script>
<!-- PNotify -->
<script src="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.js')}}"></script>
<script src="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
<script src="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{URL::to('assets/dashboard_assets/build/js/custom.min.js')}}"></script>
<script src="{{URL::to('assets/dashboard_assets/uploadfiles/js/jquery.knob.js')}}"></script>
<!-- Switchery -->
<script src="{{URL::to('assets/dashboard_assets/vendors/switchery/dist/switchery.min.js')}}" ></script>
<!-- jQuery File Upload Dependencies -->
<script src="{{URL::to('assets/dashboard_assets/uploadfiles/js/jquery.ui.widget.js')}}"></script>
<script src="{{URL::to('assets/dashboard_assets/uploadfiles/js/jquery.iframe-transport.js')}}"></script>
<script src="{{URL::to('assets/dashboard_assets/uploadfiles/js/jquery.fileupload.js')}}"></script>
<!-- Our main JS file -->

<script src="{{URL::to('assets/dashboard_assets/uploadfiles/js/script.js')}}"></script>
</body>
</html>