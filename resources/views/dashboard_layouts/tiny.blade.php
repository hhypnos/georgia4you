<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=keabx6krw69tmun56x8t2itlvls1ilozqo2vvref1ux6hqlo"></script>
<script>
    let tinyinicialization = () => {
        tinymce.init({
            forced_root_block : "",
            selector: '.content-editor',
            //a11ychecker, tinymcespellchecker , advcode , powerpaste
            plugins: 'print preview fullpage  searchreplace autolink directionality  visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount   imagetools textpattern help',
            toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | ninasGridButtonX2 | ninasGridButtonX3 | ninasGridButtonX4 | ninasGridButtonX5 | ninasGridButtonX6',
            image_advtab: true,
            images_upload_url: '{{URL::to('api/apanel/tinyphotoupload')}}',
            relative_urls: true,
            document_base_url: '{{str_replace(array("http:","https:"),"",URL::to('/'))}}',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tiny.cloud/css/codepen.min.css',
            ],
            font_formats: 'Arial Black=arial black,avant garde;Times New Roman=times new roman,times;',
            link_list: [
                {title: 'My page 1', value: 'http://www.tinymce.com'},
                {title: 'My page 2', value: 'http://www.moxiecode.com'}
            ],
            image_list: [
                {title: 'My page 1', value: 'http://www.tinymce.com'},
                {title: 'My page 2', value: 'http://www.moxiecode.com'}
            ],
            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Some class', value: 'class-name'}
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', {text: 'My text'});
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', {alt: 'My alt text'});
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', {source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg'});
                }
            },
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {
                    title: 'Some title 2',
                    description: 'Some desc 2',
                    content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>'
                }
            ],
            template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
            image_caption: true,

            spellchecker_dialog: true,
            spellchecker_whitelist: ['Ephox', 'Moxiecode'],
            max_chars: 1000000, // max. allowed chars

            content_css: "{{URL::to('assets/css/style_grid.css')}}",

            setup: function (editor) {
                let ninasMainContainerX2 = () => {
                    return '<div class="grid-object-container">\n' +
                        '    <div class="grid-object-x2">\n' +
                        '      <h1>He taught me a code. To survive.</h1>\n' +
                        '      <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '      <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '      <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '      <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x2">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '</div>';
                };
                let ninasMainContainerX3 = () => {
                    return '<div class="grid-object-container">\n' +
                        '    <div class="grid-object-x3">\n' +
                        '      <h1>He taught me a code. To survive.</h1>\n' +
                        '      <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '      <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '      <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '      <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x3">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x3">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '</div>';
                };
                let ninasMainContainerX4 = () => {
                    return '<div class="grid-object-container">\n' +
                        '    <div class="grid-object-x4">\n' +
                        '      <h1>He taught me a code. To survive.</h1>\n' +
                        '      <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '      <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '      <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '      <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x4">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x4">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x4">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '</div>';
                };
                let ninasMainContainerX5 = () => {
                    return '<div class="grid-object-container">\n' +
                        '    <div class="grid-object-x5">\n' +
                        '      <h1>He taught me a code. To survive.</h1>\n' +
                        '      <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '      <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '      <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '      <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x5">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x5">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x5">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x5">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '</div>';
                };
                let ninasMainContainerX6 = () => {
                    return '<div class="grid-object-container">\n' +
                        '    <div class="grid-object-x6">\n' +
                        '      <h1>He taught me a code. To survive.</h1>\n' +
                        '      <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '      <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '      <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '      <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x6">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x6">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x6">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                        '    <div class="grid-object-x6">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n' +
                            ' <div class="grid-object-x6">\n' +
                        '        <h1>He taught me a code. To survive.</h1>\n' +
                        '        <p>Tonight\'s the night. And it\'s going to happen again and again. It has to happen. Somehow, I doubt that. You have a good heart, Dexter. I\'m real proud of you for coming, bro. I know you hate funerals. Under normal circumstances, I\'d take that as a compliment.</p>\n' +
                        '        <p>This man is a knight in shining armor. You all right, Dexter? <strong> This man is a knight in shining armor.</strong> <em> Finding a needle in a haystack isn\'t hard when every straw is computerized.</em> Makes me a … scientist.</p>\n' +
                        '        <h2>I\'m Dexter, and I\'m not sure what I am.</h2>\n' +
                        '        <p>Tell him time is of the essence. This man is a knight in shining armor. Oh I beg to differ, I think we have a lot to discuss. After all, you are a client. Pretend. You pretend the feelings are there, for the world, for the people around you. Who knows? Maybe one day they will be.</p>\n' +
                        '\n' +
                        '    </div>\n'+
                        '</div>';
                };

                editor.ui.registry.addButton('ninasGridButtonX2', {
                    text: "Nina's Grid 2",
                    onAction: () =>{
                        editor.insertContent(ninasMainContainerX2())
                    }
                });

                editor.ui.registry.addButton('ninasGridButtonX3', {
                    text: "Nina's Grid 3",
                    onAction: () =>{
                        editor.insertContent(ninasMainContainerX3())
                    }
                });

                editor.ui.registry.addButton('ninasGridButtonX4', {
                    text: "Nina's Grid 4",
                    onAction: () =>{
                        editor.insertContent(ninasMainContainerX4())
                    }
                });

                editor.ui.registry.addButton('ninasGridButtonX5', {
                    text: "Nina's Grid 5",
                    onAction: () =>{
                        editor.insertContent(ninasMainContainerX5())
                    }
                });

                editor.ui.registry.addButton('ninasGridButtonX6', {
                    text: "Nina's Grid 6",
                    onAction: () =>{
                        editor.insertContent(ninasMainContainerX6())
                    }
                });
                // Let the editor save every change to the textarea
                editor.on('blur', function () {
                    tinymce.triggerSave();
                });
            }
        });
    };
    tinyinicialization();

</script>

