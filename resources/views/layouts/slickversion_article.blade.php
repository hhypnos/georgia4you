<div class="margin-gap">
	<div class="what-to-do">
		<div class="grida">
		@foreach($post->post_articles as $post_article)
			<div class="card-outer">
				<a href="{{URL::to('/'.Request::segment(1).'/'.Request::segment(2).'/'.$post_article->article->slug)}}">
					<div class="card" style="
							background: url('{{URL::to($post_article->article->cover_image['image']['image'])}}') no-repeat center center;
							-webkit-background-size: cover;
							-moz-background-size: cover;
							-o-background-size: cover;
							background-size: cover;
							box-shadow: inset 0px -34px 2px 200px rgba(0,0,0,0.15);
							">
						<div class="card-info-container" style="font-size: 20px !important;">
							{{$post_article->article->title}}
							<div class="card-info-border-bottom">
								<div class="card-info-border">
								</div>
							</div>
						</div>
						<div class="card-hover-info-container">
							<h2>{{$post_article->article->title}}</h2>
							<p>{{$post_article->article->description}}</p>
							<div class="plus-sign">
								<i class="fas fa-chevron-circle-right"></i>
							</div>

						</div>
					</div>
				</a>
			</div>
		@endforeach
		</div>
	</div>
</div>
