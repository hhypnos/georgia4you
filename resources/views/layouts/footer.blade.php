<footer>
	<div class="meta-description-in-footer">
		<strong style="display:block;margin-bottom:3px;clear: left;">GEORGIA 4 YOU | Best info site for travelers to Georgia</strong>
		Info site for travelers to Georgia, Caucasus. Useful info, activities, events, highlights, places to visit. Plan your road trip with ready routes and maps for drivers GEORGIA 4 YOU is a travel portal with lots of useful information and advices about travelling worldwide. Best info page about country Georgia, Tbilisi and all popular, remote and special destinations and activities in the country. Here you will find: Very useful Google map with, all important highlights in Georgia, ready routes, road maps with road difficulty frequently used by our customers that need special attention, Locations of petrol stations Travel tips and all necessary information about Georgia. Special information about driving and traveling by car: Ready road trips and driving routes for car and bike travelers Different activities and tours you can do in Georgia both in summer and winter. Under each activity or tour you will find contact info of our partner companies which offer best quality and cheapest prices The website is constantly updated and reviewed. We are always working on adding more and more interesting stuff. Enjoy browsing the site and feel free to contact us - we are working for your convenience !

	</div>



	<div class="footer">
		<div class="copyright">© 2012-2019 <img src="{{URL::to('assets/images/logo_gray.svg')}}" alt="Georgia 4 you logo for footer">
		</div>
	</div>
</footer>

<!-- Return to Top -->
<a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up"></i></a>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script>
	/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2012 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.8.0
 *
 */
	(function($, window) {
		var $window = $(window);

		$.fn.lazyload = function(options) {
			var elements = this;
			var $container;
			var settings = {
				threshold       : 0,
				failure_limit   : 0,
				event           : "scroll",
				effect          : "show",
				container       : window,
				data_attribute  : "original",
				skip_invisible  : true,
				appear          : null,
				load            : null
			};

			function update() {
				var counter = 0;

				elements.each(function() {
					var $this = $(this);
					if (settings.skip_invisible && !$this.is(":visible")) {
						return;
					}
					if ($.abovethetop(this, settings) ||
							$.leftofbegin(this, settings)) {
						/* Nothing. */
					} else if (!$.belowthefold(this, settings) &&
							!$.rightoffold(this, settings)) {
						$this.trigger("appear");
					} else {
						if (++counter > settings.failure_limit) {
							return false;
						}
					}
				});

			}

			if(options) {
				/* Maintain BC for a couple of versions. */
				if (undefined !== options.failurelimit) {
					options.failure_limit = options.failurelimit;
					delete options.failurelimit;
				}
				if (undefined !== options.effectspeed) {
					options.effect_speed = options.effectspeed;
					delete options.effectspeed;
				}

				$.extend(settings, options);
			}

			/* Cache container as jQuery as object. */
			$container = (settings.container === undefined ||
					settings.container === window) ? $window : $(settings.container);

			/* Fire one scroll event per scroll. Not one scroll event per image. */
			if (0 === settings.event.indexOf("scroll")) {
				$container.bind(settings.event, function(event) {
					return update();
				});
			}

			this.each(function() {
				var self = this;
				var $self = $(self);

				self.loaded = false;

				/* When appear is triggered load original image. */
				$self.one("appear", function() {
					if (!this.loaded) {
						if (settings.appear) {
							var elements_left = elements.length;
							settings.appear.call(self, elements_left, settings);
						}
						var loadImgUri;
						if($self.data("background"))
							loadImgUri = $self.data("background");
						else
							loadImgUri  = $self.data(settings.data_attribute);

						$("<img />")
								.bind("load", function() {
									$self
											.hide();
									if($self.data("background")){
										$self.css('backgroundImage', 'url('+$self.data("background")+')');
									}else
										$self.attr("src", $self.data(settings.data_attribute))

									$self[settings.effect](settings.effect_speed);

									self.loaded = true;

									/* Remove image from array so it is not looped next time. */
									var temp = $.grep(elements, function(element) {
										return !element.loaded;
									});
									elements = $(temp);

									if (settings.load) {
										var elements_left = elements.length;
										settings.load.call(self, elements_left, settings);
									}
								})
								.attr("src", loadImgUri );
					}

				});

				/* When wanted event is triggered load original image */
				/* by triggering appear.                              */
				if (0 !== settings.event.indexOf("scroll")) {
					$self.bind(settings.event, function(event) {
						if (!self.loaded) {
							$self.trigger("appear");
						}
					});
				}
			});

			/* Check if something appears when window is resized. */
			$window.bind("resize", function(event) {
				update();
			});

			/* Force initial check if images should appear. */
			update();

			return this;
		};

		/* Convenience methods in jQuery namespace.           */
		/* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

		$.belowthefold = function(element, settings) {
			var fold;

			if (settings.container === undefined || settings.container === window) {
				fold = $window.height() + $window.scrollTop();
			} else {
				fold = $(settings.container).offset().top + $(settings.container).height();
			}

			return fold <= $(element).offset().top - settings.threshold;
		};

		$.rightoffold = function(element, settings) {
			var fold;

			if (settings.container === undefined || settings.container === window) {
				fold = $window.width() + $window.scrollLeft();
			} else {
				fold = $(settings.container).offset().left + $(settings.container).width();
			}

			return fold <= $(element).offset().left - settings.threshold;
		};

		$.abovethetop = function(element, settings) {
			var fold;

			if (settings.container === undefined || settings.container === window) {
				fold = $window.scrollTop();
			} else {
				fold = $(settings.container).offset().top;
			}

			return fold >= $(element).offset().top + settings.threshold  + $(element).height();
		};

		$.leftofbegin = function(element, settings) {
			var fold;

			if (settings.container === undefined || settings.container === window) {
				fold = $window.scrollLeft();
			} else {
				fold = $(settings.container).offset().left;
			}

			return fold >= $(element).offset().left + settings.threshold + $(element).width();
		};

		$.inviewport = function(element, settings) {
			return !$.rightofscreen(element, settings) && !$.leftofscreen(element, settings) &&
					!$.belowthefold(element, settings) && !$.abovethetop(element, settings);
		};

		/* Custom selectors for your convenience.   */
		/* Use as $("img:below-the-fold").something() */

		$.extend($.expr[':'], {
			"below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0}); },
			"above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0}); },
			"right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0}); },
			"left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0}); },
			"in-viewport"    : function(a) { return !$.inviewport(a, {threshold : 0}); },
			/* Maintain BC for couple of versions. */
			"above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0}); },
			"right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0}); },
			"left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0}); }
		});

	})(jQuery, window);


	$(".layzy").lazyload();
	
    $('.variable-width').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });


    let anchorScroller = (selector) => {
    	event.preventDefault();
		$('html, body').animate({
			scrollTop: $(selector).offset().top-120
		}, 200);
	};

</script>
<script src="{{URL::to('assets/js/main.js')}}" charset="utf-8"></script>
</body>
</html>
