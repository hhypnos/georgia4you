@if($section->section_slug !="highlights-and-roads-map" && $section->section_slug !="contact-us")
    <div class="section-expandable-btn" onclick="expandSection(this)" id="section-btn-{{$section->section_id}}" data-sectionid="{{$section->section_id}}" data-section-name="{{strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $section->section_title))}}">
	<i class="fas fa-arrow-down"></i> <strong>{{$section->section_title}} <i class="fas fa-arrow-down"></i></strong>
</div>
@endif
<div class="section-expandable-content" id="section-{{$section->section_id}}" data-btnid="section-btn-{{$section->section_id}}">
@foreach($section->post_sections as $post_section)
    <div class="card-outer">
        <a href="{{URL::to('/'.$section->section_slug.'/'.$post_section->posts->slug)}}">
            <div class="card layzy" data-background="{{$post_section->posts->cover_image['image']['image']}}" style="
                    background-repeat:no-repeat;
                    background-position: center center ;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    box-shadow: inset 0px -34px 2px 200px rgba(0,0,0,0.15);
                    ">
                <div class="card-info-container">
                    {{$post_section->posts->title}}
                    <div class="card-info-border-bottom">
                        <div class="card-info-border">
                        </div>
                    </div>
                </div>
                <div class="card-hover-info-container">
                    <h2>{{$post_section->posts->title}}</h2>
                    <p>{{$post_section->posts->description}}</p>
                    <div class="plus-sign">
                        <i class="fas fa-chevron-circle-right"></i>
                    </div>

                </div>
            </div>
        </a>
    </div>
@endforeach
</div>

