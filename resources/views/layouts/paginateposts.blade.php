<style>
    #insta {
        background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%, #d6249f 60%, #285AEB 90%);
        -webkit-background-clip: text;
        /* Also define standard property for compatibility */
        background-clip: text;
        -webkit-text-fill-color: transparent;
    }
</style>
<div class="prev-next-post-main">
    <div class="prev-next-post-social">
        <div class="social-two" style="font-size: 25px !important;">
            <a target="_blank" href="https://www.instagram.com/georgia4you.ge/" style="color:#1da1f2">
                <i class="fab fa-instagram" id="insta"></i>
            </a>
           </div>
        <div class="social-three" style="font-size: 25px !important;">
         <a target="_blank" href="https://www.facebook.com/georgia4you.ge/" style="color: #4267b2;"><i class="fab fa-facebook-square"></i></a>
        </div>
    </div>

    <div class="prev-next-post-left"  onclick='postRedirect("{{$post_section_prev !== null ? $post_section_prev->posts->slug : $post_section_last->posts->slug }}")' data-href="{{$post_section_prev !== null ? $post_section_prev->posts->slug : $post_section_last->posts->slug }}" style="background: url('{{$post_section_prev !== null ? URL::to($post_section_prev->posts->cover_image['image']['image']) : URL::to($post_section_last->posts->cover_image['image']['image'])}}') no-repeat center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;">
        <div class="prev-next-post-left-text">
            <a href="#">PREV POST</a>
            <div class="prev-next-post-left-title">
                <h3><a href="{{$post_section_prev !== null ? $post_section_prev->posts->slug : $post_section_last->posts->slug }}">{{$post_section_prev !== null ? $post_section_prev->posts->subtitle : $post_section_last->posts->subtitle }}</a></h3>
                <h2><a href="{{$post_section_prev !== null ? $post_section_prev->posts->slug : $post_section_last->posts->slug }}">{{$post_section_prev !== null ? $post_section_prev->posts->title : $post_section_last->posts->title}}</a></h2>
            </div>
        </div>
    </div>


    <div class="prev-next-post-right uppershadow" onclick='postRedirect("{{$post_section_next !== null ? $post_section_next->posts->slug : $post_section_first->posts->slug }}")' data-href="{{$post_section_next !== null ? $post_section_next->posts->slug : $post_section_first->posts->slug }}" style="background: url('{{ $post_section_next !== null? URL::to($post_section_next->posts->cover_image['image']['image']) : URL::to($post_section_first->posts->cover_image['image']['image'])}}') no-repeat center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;">
        <div class="prev-next-post-right-text">
            <a href="{{$post_section_next !== null ? $post_section_next->posts->slug : $post_section_first->posts->slug }}">NEXT POST</a>
            <div class="prev-next-post-right-title">
                <h3><a href="{{$post_section_next !== null ? $post_section_next->posts->slug : $post_section_first->posts->slug }}">{{$post_section_next !== null ? $post_section_next->posts->subtitle : $post_section_first->posts->subtitles }}</a></h3>
                <h2><a href="{{$post_section_next !== null ? $post_section_next->posts->slug : $post_section_first->posts->slug }}">{{$post_section_next !== null ? $post_section_next->posts->title : $post_section_first->posts->title}}</a></h2>
            </div>
        </div>
    </div>
</div>
<script>
    let postRedirect = (url) => {
        window.location.href = url;
    }
</script>