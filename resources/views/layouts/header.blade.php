<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141522475-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-141522475-1');

        let anchorPageLoadScroller = () => {
            if(window.location.href.split("#")[1]){
                $('html, body').animate({
                    scrollTop: $('#'+window.location.href.split("#")[1]).offset().top-220
                }, 200);
            }
        };
        anchorPageLoadScroller();
    </script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="{{env('APP_URL')}}" />
 @foreach($languages as $language)
   <link rel="alternate" hreflang="{{$language->langAcronym}}" href="https://georgia4you.ge/language/{{$language->langID}}" />
 @endforeach
    <meta property="fb:app_id" content="1940773326029040"/>
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta property="og:url" content="{{Request::segment(2) ? Request::url() : env('APP_URL')}}" />
    <meta property="og:type" content="{{Request::segment(2) ? 'article' : 'website'}}" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:description" content="{{$description}}" />
    <meta property="og:image" content="{{Request::segment(2) ? URL::to($post->cover_image['image']['image']) : URL::to('icons/favicon-512x512.png')}}" />

    {{--<link rel="apple-touch-icon" sizes="180x180" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="32x32" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="#">--}}
    {{--<link rel="shortcut icon" type="image/ico" href="{{URL::to('assets/images/fav.ico')}}">--}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{URL::to('icons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{URL::to('icons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#a3ce54">
    <meta name="theme-color" content="#ffffff">
    {{--<link rel="mask-icon" href="" color="#a3ce54">--}}
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#a3ce54">
    <meta name="description" content="{{$description}}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{$title}}" />
    <script type="application/ld+json">
        {
          "@context": "https:\/\/schema.org",
          "@type": "WebSite",
          "url": "{{url()->full()}}",
          "name": "{{$title}}",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+(995)-557-17-11-44",
            "contactType": "Customer service"
          },
          "logo":"{{URL::to('assets/images/fav.ico')}}",
          "sameAs":["https:\/\/www.facebook.com\/georgia4you.ge"]

        }
</script>
    <meta name="google-site-verification" content="Z4YnZ7rf1gOu180sXU6iFJzCb3-IcWzRsol9o0vYiFM" />
    <title>{{$title}}</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::to('assets/css/style.css')}}" type="text/css">
</head>
<body class="loading">
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : '1940773326029040',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.3'
        });
    };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<div id="page-preloader">
    <!-- Google Chrome -->
    <div class="infinityChrome">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <!-- Safari and others -->
    <div class="infinity">
        <div>
            <span></span>
        </div>
        <div>
            <span></span>
        </div>
        <div>
            <span></span>
        </div>
    </div>

    <!-- Stuff -->
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display: none;">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="6" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                <feBlend in="SourceGraphic" in2="goo" />
            </filter>
        </defs>
    </svg>

</div>
{{--get random header image--}}
@php($header_image = $cover_image()['image'])
<header @if(url()->full() != URL::to('/')) class="alternative-header" @endif style="background: url('{{ URL::to($header_image) }}') no-repeat center center fixed;background-size: cover;">
    <div class="top-bar">
        <div class="inner-top-bar">

            <div class="mobile-menu">
                <div class="mobile-menu-btn-open" onclick="openMobileMenu(this)">
                    &#9776;
                </div>
                <div class="mobile-menu-body">
                    <div class="mobile-menu-body-content">
                        <div class="mobile-menu-btn-close" onclick="closeMobileMenu(this)">
                            &#x2716;
                        </div>
                        <div class="mobile-menu-content">
                            <div class="mobile-menu-title">
                                MENU
                            </div>

                            <div class="mobile-menu-inner-btns">
                                @if(Request::segment(2))
                                    <a href="{{URL::to('/')}}" >{{translate('Home',session('languageID'))}}<div class="short-border"></div></a>
                                    <a href="{{URL::to('/#drivingingeorgria')}}" @if(strtolower(str_replace('-','',Request::segment(1)))=="drivingingeorgia") class="mobile-menu-inner-btns-active" @endif onclick="closeMobileMenu(this)">{{translate('For Drivers',session('languageID'))}}</a>
                                    <a href="{{URL::to('/#wheretogoingeorgia')}}" @if(strtolower(str_replace('-','',Request::segment(1)))=="wheretogoingeorgia") class="mobile-menu-inner-btns-active" @endif onclick="closeMobileMenu(this)">{{translate('Where to Go',session('languageID'))}}</a>
                                    <a href="{{URL::to('/#whattodoingeorgia')}}" @if(strtolower(str_replace('-','',Request::segment(1)))=="whattodoingeorgia") class="mobile-menu-inner-btns-active" @endif onclick="closeMobileMenu(this)">{{translate('What to Do',session('languageID'))}}</a>
                                    <a href="{{URL::to('/#usefulinfo')}}" @if(strtolower(str_replace('-','',Request::segment(1)))=="usefulinfo") class="mobile-menu-inner-btns-active" @endif onclick="closeMobileMenu(this)">{{translate('Useful Info',session('languageID'))}}</a>
                                    <a href="{{URL::to('/#skiingingeorgia')}}" @if(strtolower(str_replace('-','',Request::segment(1)))=="skiingingeorgia") class="mobile-menu-inner-btns-active" @endif onclick="closeMobileMenu(this)">{{translate('Skiing',session('languageID'))}}</a>
                                    <a href="{{URL::to('/#contactus')}}" onclick="closeMobileMenu(this)">{{translate('Contact',session('languageID'))}}</a>
                                @else
                                    <a href="#" class="mobile-menu-inner-btns-active">{{translate('Home',session('languageID'))}}<div class="short-border"></div></a>
                                    <a href="#drivingingeorgria" onclick="anchorScroller('#drivingingeorgria');closeMobileMenu(this)">{{translate('For Drivers',session('languageID'))}}</a>
                                    <a href="#wheretogoingeorgia" onclick="anchorScroller('#wheretogoingeorgia');closeMobileMenu(this)">{{translate('Where to Go',session('languageID'))}}</a>
                                    <a href="#whattodoingeorgia" onclick="anchorScroller('#whattodoingeorgia');closeMobileMenu(this)">{{translate('What to Do',session('languageID'))}}</a>
                                    <a href="#usefulinfo" onclick="anchorScroller('#usefulinfo');closeMobileMenu(this)">{{translate('Useful Info',session('languageID'))}}</a>
                                    <a href="#skiingingeorgia" onclick="anchorScroller('#skiingingeorgia');closeMobileMenu(this)">{{translate('Skiing',session('languageID'))}}</a>
                                    <a href="#contactus" onclick="anchorScroller('#contactus');closeMobileMenu(this)">{{translate('Contact',session('languageID'))}}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="alt-service-logos">
                <a href="http://georgia4you.ge">
                    <div class="alt-service-logos-single-logo-containter alt-service-logos-single-logo-containter-active">
                        <img src="{{URL::to('assets/images/services-logo-georgia4you.svg')}}" alt="GEORGIA 4 YOU LOGO">
                        <div class="logo-label logo-label-active">
                            <div class="arrow-up"></div>
                            <span class="logo-label-title">GEORGIA 4 YOU</span>
                        </div>
                    </div>
                </a>

                <a href="http://cars4rent.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img src="{{URL::to('assets/images/services-logo-cars4rent.svg')}}" alt="CARS 4 RENT LOGO">
                        <div class="logo-label">
                            <div class="arrow-up"></div>
                            <span class="logo-label-title">CARS 4 RENT</span>
                        </div>
                    </div>
                </a>

                <a href="http://rooms4rent.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img src="{{URL::to('assets/images/services-logo-rooms4rent.svg')}}" alt="ROOMS 4 RENT LOGO">
                        <div class="logo-label">
                            <div class="arrow-up"></div>
                            <span class="logo-label-title">ROOMS 4 RENT</span>
                        </div>
                    </div>
                </a>

                <a href="http://support4business.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img src="{{URL::to('assets/images/services-logo-support4biz.svg')}}" alt="SUPPORO 4 BUSINESS LOGO">
                        <div class="logo-label">
                            <div class="arrow-up"></div>
                            <span class="logo-label-title">SUPPORT 4 BUSINESS</span>
                        </div>
                    </div>
                </a>

                <div class="clear"></div>
            </div>
            <div class="alt-service-logos-g4y" style="
    position: absolute;
    left: 41%;
    top: 50%;
    transform: translateY(-50%);
">
                <img src="{{URL::to('assets/images/logo_gray.svg')}}" alt="CARS 4 RENT LOGO"  class="logo-upper-image" width="210px">
            </div>
            <div class="top-bar-right-menu">


                <div class="language-chooser">
                    @foreach($languages as $language)
                        @if($language->langID ==session('languageID'))
                            <img src="{{$language->langImage}}" alt="language switch {{$language->language}}" style="width:22px;position:relative;top:2px;border:1px solid #e7e7e7;" />
                            <span>{{$language->language}}</span>
                        @endif
                    @endforeach
                    <div class="language-chooser-dropdown">
                        @foreach($languages as $language)
                            <a href="{{URL::to('language')}}/{{$language->langID}}" style="font-family: BPG_GEL_Excelsior_Caps;font-weight:normal;text-transform:uppercase">
                                <img src="{{$language->langImage}}" alt="language switch {{$language->language}}" style="width:22px;position:relative;top:2px;border:1px solid #e7e7e7;" />
                                {{$language->language}}
                            </a>
                            <br/>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="logo">
        <img src="{{URL::to('assets/images/logo.svg')}}" alt="CARS 4 RENT LOGO" class="logo-image">
    </div>
    <nav style="text-shadow: black 0px 0px 10px">

        @if(Request::segment(2))
            <a href="{{URL::to('/')}}">{{translate('Home',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="drivingingeorgia") class="active-nav-button" @endif href="{{URL::to('/#drivingingeorgia')}}">{{translate('For Drivers',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="wheretogoingeorgia") class="active-nav-button" @endif href="{{URL::to('/#wheretogoingeorgia')}}">{{translate('Where to Go',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="whattodoingeorgia") class="active-nav-button" @endif href="{{URL::to('/#whattodoingeorgia')}}">{{translate('What to Do',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="usefulinfo") class="active-nav-button" @endif href="{{URL::to('/#usefulinfo')}}">{{translate('Useful Info',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="skiingingeorgia") class="active-nav-button" @endif href="{{URL::to('/#skiingingeorgia')}}">{{translate('Skiing',session('languageID'))}}<div class="short-border"></div></a>
            <a @if(strtolower(str_replace('-','',Request::segment(1)))=="contactus") class="active-nav-button" @endif href="{{URL::to('/#contactus')}}">{{translate('Contact',session('languageID'))}}<div class="short-border"></div></a>
            @else
            <a href="#" class="active-nav-button">{{translate('Home',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#drivingingeorgria" onclick="anchorScroller('#drivingingeorgria');">{{translate('For Drivers',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#wheretogoingeorgia" onclick="anchorScroller('#wheretogoingeorgia');">{{translate('Where to Go',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#whattodoingeorgia" onclick="anchorScroller('#whattodoingeorgia');">{{translate('What to Do',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#usefulinfo" onclick="anchorScroller('#usefulinfo');">{{translate('Useful Info',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#skiingingeorgia" onclick="anchorScroller('#skiingingeorgia');">{{translate('Skiing',session('languageID'))}}<div class="short-border"></div></a>
            <a href="#contactus" onclick="anchorScroller('#contactus');">{{translate('Contact',session('languageID'))}}<div class="short-border"></div></a>
        @endif
    </nav>
    <style>
        .service-logos:hover,
        .services-logo-active {
            background: url({{$header_image}}) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <div class="slogan">
        <h1 style="font-size: 30px;text-shadow:black 0px 0px 10px">{!!translate($static_word("home",2)["word"],session('languageID')) !!} </h1>
    </div>
    <div class="services" style="text-shadow: black 0px 0px 10px">
        <div class="inner-services" style="background: url('{{$header_image}}') no-repeat center center fixed;background-size: cover;">
            <div class="service-logos services-logo-georgia4you services-logo-active">
                <div class="thick-border-left"></div>
                <div class="service-logo-button">
                    <a href="https://georgia4you.ge"><img src="{{URL::to('assets/images/services-logo-georgia4you.svg')}}" alt="GEORGIA 4 YOU LOGO"></a>
                    <div class="service-logo-button-text"><a href="https://georgia4you.ge"  style="text-decoration: none;text-transform: none;color: white;">GEORGIA 4 YOU</a></div>
                </div>
                <div class="thick-border-right"></div>
            </div>

            <div class="service-logos services-logo-cars4rent">
                <div class="thick-border-left"></div>
                <div class="service-logo-button">
                   <a href="https://cars4rent.ge" target="_blank"><img src="{{URL::to('assets/images/services-logo-cars4rent.svg')}}" alt="CARS 4 RENT LOGO"></a>
                    <div class="service-logo-button-text"><a href="https://cars4rent.ge" target="_blank" style="text-decoration: none;text-transform: none;color: white;">CARS 4 RENT</a></div>
                </div>
                <div class="thick-border-right"></div>
            </div>

            <div class="service-logos services-logo-rooms4rent">
                <div class="thick-border-left"></div>
                <div class="service-logo-button">
                    <a href="https://autos4share.com" target="_blank"><img src="{{URL::to('assets/images/services-logo-rooms4rent.svg')}}" alt="ROOMS 4 RENT LOGO"></a>
                    <div class="service-logo-button-text"><a href="https://autos4share.com" target="_blank" style="text-decoration: none;text-transform: none;color: white;">AUTOS 4 SHARE</a></div>
                </div>
                <div class="thick-border-right"></div>
            </div>

            <div class="service-logos services-logo-support4biz">
                <div class="thick-border-left"></div>
                <div class="service-logo-button">
                    <a href="https://support4business.ge"><img src="{{URL::to('assets/images/services-logo-support4biz.svg')}}" alt="SUPPORT 4 BIZ LOGO"></a>
                    <div class="service-logo-button-text"><a href="https://support4business.ge" target="_blank" style="text-decoration: none;text-transform: none;color: white;">SUPPORT 4 BIZ</a></div>
                </div>
                <div class="thick-border-right"></div>
            </div>

            <div class="line-through-services"></div>
        </div>

        <div class="line-through-services"></div>

    </div>

</header>