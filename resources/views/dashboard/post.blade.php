@extends('dashboard_layouts.master')
@section('content')
    <style>
        .grid-object-container {
            width:100%;
            display: flex;
            flex-wrap: wrap;
        }
        .grid-object-container .grid-object-x2{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:50%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x3{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:33.33%;
            float:left;
            padding:20px;
        }

        .grid-object-container .grid-object-x4{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:25%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x5{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:20%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x6{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:16.66%;
            float:left;
            padding:20px;
        }

        @media(max-width:800px){
            .grid-object-container .grid-object-x2{
                width:50%;
            }
            .grid-object-container .grid-object-x3{
                width:50%;
            }
            .grid-object-container .grid-object-x4{
                width:50%;
            }
            .grid-object-container .grid-object-x5{
                width:50%;
            }
            .grid-object-container .grid-object-x6{
                width:50%;
            }
        }
        @media(max-width:500px){
            .grid-object-container .grid-object-x2{
                width:100%;
            }
            .grid-object-container .grid-object-x3{
                width:100%;
            }
            .grid-object-container .grid-object-x4{
                width:100%;
            }
            .grid-object-container .grid-object-x5{
                width:100%;
            }
            .grid-object-container .grid-object-x6{
                width:100%;
            }
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="wholecontent">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{$post->title}}</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$post->subtitle}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <style>
                                .checkbox{
                                    display: inline-block;
                                }
                            </style>
                            <form class="form-horizontal form-label-left" id="updatepost" method="post" action="{{URL::to('apanel/post/updatepost')}}">
                                {{csrf_field()}}
                                @php($draft=true)
                                @php($link = "drafts")
                                @php($get_section = null)
                                    @foreach($sections as $section)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="sections[]" id="section-{{$section->section_id}}" value="{{$section->section_id}}" class="flat"
                                                       {{--find if already has secion--}}
                                                @if(in_array($section->section_id,$post->inSections->pluck("section_id")->toArray()))
                                                    @php($draft=false)
                                                    @php($link = "section")
                                                    @php($get_section = $section->section_slug)
                                                    checked
                                                @endif
                                                /> {{$section->section_title}}
                                            </label>
                                        </div>
                                    @endforeach
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="sections[]" id="section-draft" value="draft" class="flat"  disabled checked/> Draft
                                        <input type="hidden" name="sections[]" value="draft" class="flat" checked/>
                                    </label>
                                </div>

                                 <br><br>
                                <span class="section">Data</span>
                                        <input type="hidden" name="post_id" value="{{$post->id}}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" value="{{$post->title}}" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Subtitle <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="subtitle" required="required"  value="{{$post->subtitle}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="description" required="required" name="description" class="form-control col-md-7 col-xs-12">{{$post->description}}</textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">MetaDescription <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="metaDescription" required="required" name="metaDescription" class="form-control col-md-7 col-xs-12">{{$post->metaDescription}}</textarea>
                                            </div>
                                        </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor" class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12">{!! $post->text !!}</textarea>
                                    </div>
                                </div>
                                <br><br><br>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Slug <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="slug" name="slug" required="required" placeholder=""  value="{{$post->slug}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button  id="cancel" onclick="event.preventDefault();(function(){window.history.back();})();" class="btn btn-primary">Cancel</button>
                                        <a href="{{route('translated_post',array($post->posts_id,1))}}" class="btn btn-warning">Translate</a>

                                        <button id="send" type="submit"class="btn btn-success">Submit</button>
                                        <button id="delete" onclick="event.preventDefault();(function(){$('#my-confirm-dialog').show(); })();" class="btn btn-danger">Delete</button>
                                    </div>

                                </div>
                            </form>
                            <br><br><br><br><br>
                            <h1>Texts</h1>
                            @foreach($post->post_texts as $post_text)
                            <form class="form-horizontal form-label-left" method="post" data-uniqueid="id-{{$post_text->post_textID}}" onsubmit="updateText(this)">
                                {{csrf_field()}}
                                <input type="hidden" name="post_text_id" value="{{$post_text->post_textID}}">
                                <input type="hidden" name="image_genderID" value="1">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" value="{{$post_text->text->title}}" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor" class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12">{{$post_text->text->text}}</textarea>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="leftOrRight" id="leftOrRight">
                                            <option value="left" @if($post_text->text->leftOrRight == 'left') selected @endif>left</option>
                                            <option value="right" @if($post_text->text->leftOrRight == 'right') selected @endif>right</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" name="order_id" value="{{$post_text->order_id}}" type="number">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Show in breadcrumbs <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label>
                                            <input type="checkbox" name="breadcrumbs" value="2" class="js-switch" @if($post_text->text->breadcrumbs == 2) checked @endif/>
                                        </label>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Images <span class="required">*</span>
                                    </label>
                                    <input type="hidden" name="image_genderID" value="1">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="btn btn-danger btn-file">
                                            <i class="fa fa-file-o"></i> Choose File <input type="file" id="posttextfile" name="upl[]" style="display:none" multiple="" class="form-control col-md-7 col-xs-12">
                                            <div id="selectedFiles"></div>
                                        </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                    </div>

                                </div>
                                <button type="submit" class="btn btn-success" id="submitpublishedposttext" >Submit</button>
                                <a href="{{route('translated_text',array($post_text->text->post_texts_id,1))}}" class="btn btn-warning">Translate</a>
                                <a href="#" class="btn btn-danger" data-posttextid='{{$post_text->text->post_textID}}' id="deletepublishedposttext" onclick="deletepublishedposttext(this)">Delete</a>
                                @foreach($post_text->text->text_images as $text_image)
                                    <div class="item form-group deletepublishedposttextimage">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Image <span class="required">*</span>
                                        </label>
                                        <div class="col-md-55">
                                                <div class="thumbnail">
                                                    <div class="image view view-first">
                                                        <a href="#"><img style="width: 100%; display: block;" src="{{URL::to($text_image->image_data->image)}}" alt="image" /></a>
                                                        <div class="mask">
                                                            <div class="tools tools-bottom">
                                                                <a target="_blank" href="{{URL::to($text_image->image_data->image)}}"><i class="fa fa-link"></i></a>
                                                                <a data-textimageid='{{$text_image->id}}' onclick="return confirm('Are you sure you want to delete this post from section ?')? deletepublishedposttextimage(this) : '' "><i class="fa fa-times"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                @endforeach
                            </form>
                            @endforeach
                            <br><br><br><br><br><br>
                            <h1>Insert Texts</h1>
                            <hr>
                            {{--NEW FORM --}}
                            <div style="font-size:50px;position: fixed;top: 30%;cursor:pointer" onclick="insertForm()">+</div>
                            <div id="forminserter">
                                <form class="form-horizontal form-label-left newforminserttext"  id="newforminserttext" method="post">
                                    <div style="font-size: 60px;cursor: pointer;width: 18px;margin-left: 50%;height: 57px;" onclick="deleteForm(this)">-</div>
                                    <br><br><br>
                                {{csrf_field()}}
                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                <input type="hidden" name="image_genderID" value="1">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="leftOrRight" id="leftOrRight">
                                            <option value="right">right</option>
                                            <option value="left">left</option>
                                        </select>
                                    </div>
                                </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Show in breadcrumbs <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>
                                                <input type="checkbox" name="breadcrumbs" value="2" class="js-switch"/>
                                            </label>
                                        </div>
                                    </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Images <span class="required">*</span>
                                    </label>
                                    <input type="hidden" name="image_genderID" value="1">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="btn btn-danger btn-file">
                                            <i class="fa fa-file-o"></i> Choose File <input type="file" id="posttextfile" name="upl[]" style="display:none" multiple="" class="form-control col-md-7 col-xs-12">
                                            <div id="selectedFiles"></div>
                                        </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                    </div>

                                </div>
                                    <br><br><br><br>
                            </form>

                            </div>
                            <button class="btn btn-success" id="submitallform" onclick="submitInsertText()">Submit</button>
                            <hr>
                        </div>
                        <div class="x_content2">

                            <div class="row">
                                @foreach($post->all_images as $image)
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first" style="height: 100%;">
                                            <img style="width: 100%; display: block;" src="{{URL::to('/'.$image['image']['image'])}}" alt="image" />
                                            <div class="mask">
                                                <input type="number" data-imageid="{{$image->imageID}}"  data-uploadurl="{{URL::to('apanel/post/changeimageorder')}}" onchange="changeorder(this)" style="color:black;width:15%; margin-top:5px" value="{{$image->order_id}}" >
                                                <div class="tools tools-bottom">
                                                <a href="{{URL::to('/'.$image['image']['image'])}}" target="_blank"><i class="fa fa-link"></i></a>
                                                    <a href="#" ><i class="fa fa-star" data-imageid="{{$image->imageID}}"  data-contentid="{{$post->id}}" onclick="makedefaultimage(this)" style="{{$image->defaultOrNot == 2 ? 'color:red':'color:white'}}"></i></a>
                                                    <a href="#"><i data-imageid="{{$image->imageID}}" data-contentid="{{$post->id}}" data-deleteurl="{{URL::to('apanel/post/deleteimage')}}" onclick="deleteimagefrompost(this)" class="fa fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <form id="upload" method="post" action="{{URL::to('apanel/post/uploadimage')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <input type="hidden" name="image_genderID" value="1">
                <div id="drop">
                    Drop Here

                    <a>Browse</a>

                    <input type="file" name="upl[]" multiple />
                </div>

                <ul>
                    <!-- The file uploads will be shown here -->
                </ul>
            </form>
            <br><br><br>
            <form id="sliderupload" method="post" action="{{URL::to('apanel/post/uploadsliderimage')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <div id="drop_slider">
                    Make SLider

                    <a>Browse</a>

                    <input type="file" name="upl[]" multiple />
                </div>

                <ul>
                    <!-- The file uploads will be shown here -->
                </ul>
            </form>
            <div class="row">
                @foreach($post->slider as $image)
                    <div class="col-md-55">
                        <div class="thumbnail">
                            <div class="image view view-first" style="height: 100%;">
                                <img style="width: 100%; display: block;" src="{{URL::to('/'.$image->fetch_image->image)}}" alt="image" />
                                <div class="mask">
                                    <input type="number" data-imageid="{{$image->imageID}}"  data-uploadurl="{{URL::to('apanel/post/ordersliderimage')}}" onchange="changeorder(this)" style="color:black;width:15%; margin-top:5px" value="{{$image->order_id}}" >
                                    <div class="tools tools-bottom">
                                        <a href="{{URL::to('/'.$image->fetch_image->image)}}" target="_blank"><i class="fa fa-link"></i></a>
                                        <a href="#"><i data-imageid="{{$image->imageID}}" data-deleteurl="{{URL::to('apanel/post/deletesliderimage')}}" data-contentid="{{$post->id}}" onclick="deleteimagefrompost(this)" class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <br><br><br><br><br>
            <div class="row">
                @foreach($post->post_articles as $post_article)
                    <div class="col-md-55">
                        <div class="thumbnail">
                            <div class="image view view-first">
                                <a href="#"><img style="width: 100%; display: block;" src="{{URL::to($post_article->article->cover_image['image']['image'])}}" alt="image" /></a>
                                <div class="mask">
                                    <input type="number" data-articlename="{{$post_article->article->title}}" data-postname="{{$post->title}}" data-articleid="{{$post_article->articleID}}" data-postid="{{$post_article->postID}}" onchange="changeorderArticle(this)" style="color:black;width:15%; margin-top:5px" value="{{$post_article->order_id}}" >
                                    <div class="tools tools-bottom">
                                        <a href="{{URL::to('/'.$get_section.'/'.$post->slug.'/'.$post_article->article->slug)}}" target="_blank"><i class="fa fa-link"></i></a>
                                        <a href="{{URL::to('apanel/post/')}}/{{$post_article->article->slug}}"><i class="fa fa-pencil"></i></a>
                                        <a href="#" data-postname="{{$post_article->article->title}}" data-articleid="{{$post_article->article->id}}" data-postid="{{$post_article->postID}}" onclick="return confirm('Are you sure you want to delete this post from section ?')? deleteArticleFromPost(this) : '' "><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="caption">
                                <a href="{{URL::to('apanel/post/')}}/{{$post_article->article->slug}}"><p>{{$post_article->article->title}}<br>{{$post_article->article->description}}</p></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="my-confirm-dialog" class="dialog-overlay">

        <div class="dialog-card">

            <div class="dialog-question-sign"><i class="fa fa-question"></i></div>

            <div class="dialog-info">

                <h5>Are you sure?</h5>
                <p>Post Can't be restored</p>

                <button class="dialog-confirm-button" onclick="deletePost()">Yes</button>
                <button class="dialog-reject-button" onclick='(function(){$("#my-confirm-dialog").hide(); })();'>No</button>

            </div>

        </div>

    </div>

    <script>
        $("form").submit((e) => {
            e.preventDefault();
        });

        let deletepublishedposttextimage = (element)=>{
            event.preventDefault();
            if(!confirm('Are you sure you want to delete this image?')){
                return;
            }
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/deleteposttextimage')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token:'{{csrf_token()}}', imageID:$(element).data("textimageid")},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //remove input
                    $(element).closest(".deletepublishedposttextimage").remove();
                    new PNotify({
                        title: 'Post ',
                        text: 'Post Text Image Removed',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        let deletepublishedposttext = (element)=>{
            event.preventDefault();
            if(!confirm('Are you sure you want to delete this text?')){
                return;
            }
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/deleteposttext')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: $(element).closest("form").serialize(),
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //remove input
                    $(element).closest("form").remove();
                    new PNotify({
                        title: 'Post ',
                        text: 'Post Text Removed',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        let insertForm = () => {
            $( "#forminserter" ).append('<form class="form-horizontal form-label-left newforminserttext"  id="newforminserttext" method="post">\n' +
                '                                    <div style="font-size: 60px;cursor: pointer;width: 18px;margin-left: 50%;height: 57px;" onclick="deleteForm(this)">-</div>\n' +
                '                                    <br><br><br>\n' +
                '                                {{csrf_field()}}\n' +
                '                                <input type="hidden" name="post_id" value="{{$post->id}}">\n' +
                '                                <input type="hidden" name="image_genderID" value="1">\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" type="text">\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <textarea class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12"></textarea>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <select name="leftOrRight" id="leftOrRight">\n' +
                '                                            <option value="right">right</option>\n' +
                '                                            <option value="left">left</option>\n' +
                '                                        </select>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                                    <div class="item form-group">\n' +
                '                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Show in breadcrumbs <span class="required">*</span>\n' +
                '                                        </label>\n' +
                '                                        <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" name="breadcrumbs" value="2" class="js-switch"/>\n' +
                '                                            </label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Images <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <input type="hidden" name="image_genderID" value="1">\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <label class="btn btn-danger btn-file">\n' +
                '                                            <i class="fa fa-file-o"></i> Choose File <input type="file" id="posttextfile" name="upl[]" style="display:none" multiple="" class="form-control col-md-7 col-xs-12">\n' +
                '                                            <div id="selectedFiles"></div>\n' +
                '                                        </label>\n' +
                '                                    </div>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                                    <br><br><br><br>\n' +
                '                            </form>');
            tinyinicialization();


        };

        let deleteForm = (element) => {
            if($(".newforminserttext").length>1) {
                $(element).closest( "form" ).remove();
            }
        };

        let submitInsertText = () => {
            let i =0;
            while(i<$('.newforminserttext').length){
                console.log(i);
                insertText(i);
                i++;
            }
        }
      let insertText = (element) => {
          let formData = new FormData($('.newforminserttext')[element]);
          formData.append('csrfmiddlewaretoken', '{{ csrf_token() }}');
          console.log(formData)
          $.ajax({
              /* the route pointing to the post function */
              url: '{{URL::to('apanel/post/insertposttext')}}',
              type: 'POST',
              /* send the csrf-token and the input to the controller */
              data: formData,
              async: true,
              cache:false,
              contentType: false,
              processData: false,
              /* remind that 'data' is the response of the AjaxController */

              success: function (data) {
                  if(element == 0){
                      $('.newforminserttext')[element].reset();
                  }else {
                      $('.newforminserttext')[element].remove();
                  }
                  //refresh section
                  new PNotify({
                      title: 'Post ',
                      text: 'Post text Inserted ',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
              },

              error: function (request, status, error) {
                  for (x in request.responseJSON['errors']) {
                      new PNotify({
                          title: 'Error',
                          text: request.responseJSON['errors'][x],
                          type: 'error',
                          styling: 'bootstrap3'
                      });
                  }
              }

          });
       };

        let updateText = (element) => {
            let fullForm = $(element)[0];
            console.log(fullForm);
            let formData = new FormData(fullForm);
            formData.append('csrfmiddlewaretoken', '{{ csrf_token() }}');
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/updateposttext')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post ',
                        text: 'Post text updated ',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        function deletePost() {
                $(".wholecontent").hide();
                $(".x_content2").hide();
                $("#my-confirm-dialog").hide();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{URL::to('apanel/post/deletepost')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: $("#updatepost").serialize(),
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Post Deleted ',
                            text: 'Post Deleted <a href={{URL::to('apanel/'.$link)}}>go back </a>',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }
            document.getElementById("send").onclick=function (e) {

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{URL::to('apanel/post/updatepost')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: $("#updatepost").serialize(),
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Post Updated ',
                            text: 'Post Updated <a class="btn btn-dark" href="{{URL::to('/')}}/{{$post->slug}}" target="_blank">Preview</a>',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }

            function deleteimagefrompost(element) {
                var CSRF_TOKEN = "{{csrf_token()}}";
                $.ajax({
                    /* the route pointing to the post function */
                    url: $(element).data('deleteurl'),
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), contentID:$(element).data('contentid')},
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Image Deleted ',
                            text: '',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        refreshContent()
                    },

                    error: function (request, status, error) {
                        new PNotify({
                            title: 'Error',
                            text: 'There are only 1 image , so you can"t delete default image',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                });
            }

        function makedefaultimage(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/makedefaultimage')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), contentID:$(element).data('contentid')},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Image Set Default ',
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent()>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        }

        function changeorder(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: $(element).data('uploadurl'),
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), orderID:$(element).val()},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Image Order Updated ',
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent()>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        }

        let changeorderArticle = (element) => {
            var CSRF_TOKEN = "{{csrf_token()}}";

            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/changeorderarticle')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), articleID: $(element).data('articleid') ,orderID:$(element).val()},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Order Updated in '+$(element).data('postname'),
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('postid')+')>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }

        let deleteArticleFromPost = (element) => {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/deletearticlefrompost')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), articleID: $(element).data('articleid') },
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post Deleted',
                        text: 'Post '+$(element).data("postname")+' Drafted Successfully',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $( "#post-"+$(element).data('postid') ).load(window.location.href + " #post-"+$(element).data('postid') );

                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }
        function refreshContent() {
            $( ".x_content2").load(window.location.href + " .x_content2");
        }

        $(function(){
            $('#posttextfile').change(function(){
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push('<li>' + $(this).get(0).files[i].name + '</li>');
                }
                $("#selectedFiles").html(names);
            });
        });
    </script>
    <!-- /page content -->
@endsection