@extends('dashboard_layouts.master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> {{$title}} </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        @foreach($sections as $section)
        <div class="row" id="section-{{$section->section_id}}">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h5>{{$section->subtitle_up}}</h5>
                        <h1>{{$section->section_title}}</h1>
                        <h5> {!! $section->subtitle_down !!} </h5>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($section_styles as $section_style)
                                        <li>
                                            <button class="btn btn-primary" style="width:100%" data-sectionid="{{$section->section_id}}" data-sectionstyleid="{{$section_style->section_style_id}}" onclick="applystyle(this)">{{$section_style->section_style}}</button>
                                        </li>
                                    @endforeach
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">
                            @foreach($section->post_sections as $post_section)
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <a href="#"><img style="width: 100%; display: block;" src="{{URL::to($post_section->posts->cover_image['image']['image'])}}" alt="image" /></a>
                                        <div class="mask">
                                           <input type="number" data-postname="{{$post_section->posts->title}}" data-sectionname="{{$section->section_title}}" data-postid="{{$post_section->postID}}" data-sectionid="{{$post_section->section_id}}" onchange="changeorder(this)" style="color:black;width:15%; margin-top:5px" value="{{$post_section->order_id}}" >
                                            <div class="tools tools-bottom">
                                                <a href="{{URL::to('/'.$section->section_slug.'/'.$post_section->posts->slug)}}" target="_blank"><i class="fa fa-link"></i></a>
                                                <a href="{{URL::to('apanel/post/')}}/{{$post_section->posts->slug}}"><i class="fa fa-pencil"></i></a>
                                                <a href="#" data-postname="{{$post_section->posts->title}}" data-postid="{{$post_section->posts->id}}" data-sectionid="{{$post_section->section_id}}" onclick="return confirm('Are you sure you want to delete this post from section ?')? deletepostfromsection(this) : '' "><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <a href="{{URL::to('apanel/post/')}}/{{$post_section->posts->slug}}"><p>{{$post_section->posts->title}}<br>{{$post_section->posts->description}}</p></a>
                                        @if(!$post_section->posts->where('posts_id',$post_section->posts->id)->where('langID',3)->exists() || $post_section->posts->where('posts_id',$post_section->posts->id)->where('langID',3)->first()['updated_at']<$post_section->posts->updated_at)
                                            <h4 style="position: absolute;top:0px;color:red;">Neeed translate on Russian</h4>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<!-- /page content -->
<script type="text/javascript">
    $("form").submit((e) => {
        e.preventDefault();
    });
    function deletepostfromsection(element){
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/deletepostfromsection')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') },
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Post Deleted',
                    text: 'Post '+$(element).data("postname")+' Drafted Successfully',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $( "#section-"+$(element).data('sectionid') ).load(window.location.href + " #section-"+$(element).data('sectionid') );

            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }

    function changeorder(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/changeorder')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') ,orderID:$(element).val()},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Order Updated in '+$(element).data('sectionname'),
                    text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }



    function applystyle(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/sectionapplystyle')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, sectionstyleID: $(element).data('sectionstyleid'), sectionID: $(element).data('sectionid')},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Style changed '+$(element).data('sectionname'),
                    text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }

    function refreshContent(sectionid) {
        $( "#section-"+sectionid).load(window.location.href + " #section-"+sectionid);
    }
</script>
@endsection