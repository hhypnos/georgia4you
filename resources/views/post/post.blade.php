    @extends('layouts.master')
@section('content')
    <style>
      main .text{
            font-size: 13px !important;
            line-height: 20px;
        }
    </style>
    <main style="margin-top: 125px">
        <div class="breadcrumb-first">
            @php($i=1)
            @while(Request::segment($i) !== null)
                @if($i == 1)
                    <a class="breadcrumb__step " href="{{URL::to('/')}}"><i class="fas fa-home" style="font-size:15px;"></i></a>
                @else
                    <a class="breadcrumb__step " href="#">{{strtoupper(Request::segment($i))}}</a>
                @endif
                @php($i++)
            @endwhile
        </div>
        <br>
        <div class="breadcrumb">
        @foreach($post->post_texts as $post_text)
			@if(!is_null($post_text->text))
					@if($post_text->text->breadcrumbs == 2)
						<a class="breadcrumb__step " href="#{{strtolower(str_replace(array(' ',',','&','.'),'',$post_text->text->title))}}" onclick="anchorScroller('#{{strtolower(str_replace(array(' ',',','&','.'),'',$post_text->text->title))}}');">{{$post_text->text->title}}</a>
					@endif
			@endif
        @endforeach
        </div>
        <br><br>
        <style>
            .slick-slide img {
                display: block;
                height: 400px;
                height: 400px;
                margin-left: 30px;
            }
            .slick-dots{
                display: none !important;
            }
            .far {
                font-size: 30px;
            }
        </style>
        <div class="variable-width">
            @foreach($post->slider as $slider)
                <div><img src="{{URL::to($slider->fetch_image->image)}}" alt="{{$slider->fetch_image['alt']}}"></div>
            @endforeach
        </div>
        <br><br>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=1940773326029040&autoLogAppEvents=1"></script>
		<div class="fb-share-button" data-href="{{Request::fullUrl()}}" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
        <h3 style="font-size: 14px;font-weight: lighter; text-transform: uppercase">{{$post->subtitle}}</h3>
        <h2 style="font-size:45px;font-weight: 700;text-transform: uppercase;margin:0">{{$post->title}}</h2>
        <br>
        <br>
        <br>
        <div class="maintextcontainer" style="border-bottom: 5px dotted #a3ce54;margin-bottom: 33px;font-size: 13px;font-size: 13px;line-height: 1.7;">
        {!! $post->text !!}
        </div>
        <div class="maintextcontainer" @if(!$post->post_texts->isEmpty()) style="border-bottom: 5px dotted #a3ce54;margin-bottom: 33px;" @endif>
        @foreach($post->post_texts as $post_text)
			@if(!is_null($post_text->text))
        <div class="grid-container"  id="{{strtolower(str_replace(array(' ',',','&','.'),'',$post_text->text->title))}}" @if($post_text->text->text_images->isEmpty()) style="position: relative;margin-top: 0px;padding-bottom: 19px;display: block;" @endif>
            <div @if(!$post_text->text->text_images->isEmpty()) class="post-template-{{$post_text->text->leftOrRight}}" @endif style="grid-column: 1/span 2;position:relative;">
                <div class="card" style=" background: url('{{!$post_text->text->text_images->isEmpty() ? URL::to($post_text->text->text_images[0]->image_data->image) : ''}}') no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;{{$post_text->text->text_images->isEmpty() ? 'margin-bottom: -14px' : ''}};">
                    <div class="white-border-simulator"></div>
                    <!--<div class="title" style="line-height: 40px !important;width:100%; @if($post_text->text->text_images->isEmpty())left: 0;@endif ">
                        <h2 style="position:relative;margin:0 auto;font-size: 35px;" class="{{$post_text->text->leftOrRight =='left' ? 'heading-right' : 'heading-left'}}">{!! $post_text->text->title !!}
                        <div style="border-bottom: 4px solid #a3ce54;width:50px;" class="{{$post_text->text->leftOrRight =='left' ? 'green-dash-left' : 'green-dash-right'}}"></div>
						</h2>
                    </div>-->

                </div>
				<div class="title-outer {{$post_text->text->text_images->isEmpty() ? 'no-image' : ''}}">
					<div class="title">
						<h2 class="{{$post_text->text->leftOrRight =='left' ? 'heading-right' : 'heading-left'}}">
							{!! $post_text->text->title !!}
							<div class="{{$post_text->text->leftOrRight =='left' ? 'green-dash-left' : 'green-dash-right'}}"></div>
						</h2>
						
					</div>
				</div>

                <div style="clear:both"></div>
                @if(!$post_text->text->text_images->isEmpty())
                <div class="box-simulator"></div>
                @endif
                <div class="text @if($post_text->text->text_images->isEmpty()) padding-needed @endif">
                   {!! $post_text->text->text !!}
                </div>

            </div>
        </div>
				@endif
        @endforeach
			<div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="10"></div>
        </div>
        @include('layouts.slickversion_article')

        @include('layouts.paginateposts')
    </main>
    <style>
        .grid-container {
            display: grid;
            grid-template-columns: 1fr 1.5fr;
            grid-template-rows: 0.45fr 0.537fr;
            grid-template-areas: "post-template post-template" "post-template post-template";
            font-size:12px;
            font-family: 'Sansation';
            text-align: justify;
        }

        .post-template-right, .post-template-left { grid-area: post-template; }

        .post-template-right .text{
            position: relative;
            top: -60px;
            left:16px;
            font-size: 15px;
            text-align: justify;
            line-height: 1.7;
        }
        .post-template-right .box-simulator{
           position: relative;
            top: -70px;
            float: left;
            height: 71px;
            width: 334px;
        }
		
		
		.title-outer{
			height: 225px;
			position: relative;
		}
		
		.title-outer.no-image{
			height:auto;
            font-size: 24px;
		}

	
		.padding-needed{
			padding-top:33px !important;
		}

		@media (min-width:1000px){
			.title-outer.no-image{
				height:20px !important;
			}
		}
		@media (max-width:1000px){
			.padding-needed{
				padding-top:10px !important;
			}
		}

		.post-template-right .green-dash-right{
			height: 5px;
			width: 50px;
			background: #a3ce54;
			position: absolute;
			bottom: 23px;
			left: 0;
		}
        .no-image .green-dash-right {
            height: 5px;
            width: 50px;
            background: #a3ce54;
            position: absolute;
            left: 0;
        }

		.post-template-left .green-dash-left{
			height: 5px;
			width: 50px;
			background: #a3ce54;
			position: absolute;
			bottom: 23px;
			right: 0;
		}
		
        .post-template-right .title{
			font-size: 24px !important;
			line-height: 35px;
			position: absolute;
			left: 610px;
			bottom: 0;
        }
		.post-template-left .title {
			font-size: 24px !important;
			line-height: 35px;
			position: absolute;
			right: 610px;
			bottom: 0;
        }
		@media (max-width:1000px){
			.post-template-right .green-dash-right{
				height: 5px;
				width: 50px;
				background: #a3ce54;
				position: absolute;
				bottom: 23px;
				left: 0;
			}
			.post-template-left .green-dash-left{
				height: 5px;
				width: 50px;
				background: #a3ce54;
				position: absolute;
				bottom: 23px;
				left: 0;
			}
			
			.post-template-right .title{
				font-size: 24px !important;
				line-height: 35px;
				position: absolute;
				left: 10px;
				bottom: 0;
				color:white;
			}
			.post-template-left .title {
				font-size: 24px !important;
				line-height: 35px;
				position: absolute;
				left: 10px;
				bottom: 0;
				color:white;
			}
		}
		
		
        .post-template-right .white-border-simulator{
            border: 15px solid white;
            height: 129px;
            width: 542px;
            background: white;
            position: absolute;
            bottom: -44px;
            left: 318px;
            border-radius: 35px;
        }
		@media (max-width:1000px){
			.post-template-right .white-border-simulator,
			.post-template-left .white-border-simulator{
				display:none;
			}
		}
        .post-template-right .card{
            width: 580px;
            float: left;
            height: 310px;
            /* grid-column: 1/span 2; */
            border-radius: 15px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .post-template-left .card{
            width: 580px;
            float: right;
            height: 310px;
            /* grid-column: 1/span 2; */
            border-radius: 15px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .post-template-left .white-border-simulator {
            border: 15px solid white;
            height: 129px;
            width: 542px;
            background: white;
            position: absolute;
            bottom: -44px;
            left: -279px;
            border-radius: 35px;
        }

        .post-template-left .box-simulator {
            position: relative;
            top: -70px;
            float: right;
            height: 71px;
            width: 334px;
        }
        .post-template-left .text{
            position: relative;
            top: -60px;
            right: 16px;
            text-align: right;
            line-height: 1.7;
        }
		@media (max-width:1000px){
			.post-template-right .text,
			.post-template-left .text{
				top: 0;
				left: 0;
				right:0;
			}
			.box-simulator{
				display:none;
			}
			.post-template-right .title{
				
			}
			.post-template-left .title{
				
			}
		}
		
		.heading-left{
			text-align:left;
		}
		.heading-right{
			text-align:right;
		}

		
        @media only screen and (max-width : 1000px) {
			.title{
				width:100%;
			}
			main{
				width: 100%;
			}
			.grid-container{
				margin-bottom:40px;
			}
			.heading-left{
				text-align:left;
			}
			.heading-right{
				text-align:left;
			}
            /* Styles */
            .post-template-right .title{
                
            }
            .post-template-right .card{
                width: 100% !important;
            }

            /*left side*/

            .post-template-left .card{
                width: 100% !important;
            }

            .post-template-left .white-border-simulator {
                border: 15px solid white;
                height: 129px;
                width: 542px;
                background: white;
                position: absolute;
                bottom: -44px;
                left: -205px;
                border-radius: 35px;
            }
        }




        /* ------------------------- Separate line ------------------------- */
		@media (min-width:1230px){
			:root {
				--breadcrumb-theme-1: #a3ce54;
				--breadcrumb-theme-2: #fff;
				--breadcrumb-theme-3: #a3ce54;
				--breadcrumb-theme-4: #a3ce54;
			}
			.breadcrumb {
				text-align: center;
				display: inline-block;
				box-shadow: 0 2px 5px rgba(0,0,0,0.25);
				overflow: hidden;
				border-radius: 5px;
				counter-reset: flag;
				margin-bottom: 5px;
			}
			.breadcrumb__step {
				text-decoration: none;
				outline: none;
				display: block;
				float: left;
				font-size: 12px;
				line-height: 36px;
				padding: 0 10px 0 30px;
				position: relative;
				background: var(--breadcrumb-theme-2);
				color: var(--breadcrumb-theme-1);
				transition: background 0.5s;
			}
			.breadcrumb__step:first-child {
				padding-left: 10px;
				border-radius: 5px 0 0 5px;
			}
			.breadcrumb__step:first-child::before {
				left: 14px;
			}
			.breadcrumb__step:last-child {
				border-radius: 0 5px 5px 0;
				padding-right: 20px;
			}
			.breadcrumb__step:last-child::after {
				content: none;
			}
			.breadcrumb__step::before {
				/*content: counter(flag);*/
				/*counter-increment: flag;*/
				border-radius: 100%;
				width: 20px;
				height: 20px;
				line-height: 20px;
				margin: 8px 0;
				position: absolute;
				top: 0;
				left: 30px;
				font-weight: bold;
				background: var(--breadcrumb-theme-2);
				box-shadow: 0 0 0 1px var(--breadcrumb-theme-1);
			}
			.breadcrumb__step::after {
				content: '';
				position: absolute;
				top: 0;
				right: -18px;
				width: 36px;
				height: 36px;
				transform: scale(0.707) rotate(45deg);
				z-index: 1;
				border-radius: 0 5px 0 50px;
				background: var(--breadcrumb-theme-2);
				transition: background 0.5s;
				box-shadow: 2px -2px 0 2px var(--breadcrumb-theme-4);
			}
			/*.breadcrumb__step:first-child {*/
				/*color: var(--breadcrumb-theme-2);*/
				/*background: var(--breadcrumb-theme-1);*/
			/*}*/
			/*.breadcrumb__step:first-child::before {*/
				/*color: var(--breadcrumb-theme-1);*/
			/*}*/
			/*.breadcrumb__step:first-child::after {*/
				/*background: var(--breadcrumb-theme-1);*/
			/*}*/
			.breadcrumb__step:hover {
				color: var(--breadcrumb-theme-2);
				background: var(--breadcrumb-theme-3);
			}
			.breadcrumb__step:hover::before {
				color: var(--breadcrumb-theme-1);
			}
			.breadcrumb__step:hover::after {
				color: var(--breadcrumb-theme-1);
				background: var(--breadcrumb-theme-3);
			}

		}
		.breadcrumb a{
			display:block;
			color: #a3ce54;
			text-decoration:none;
			margin-bottom:0px;
		}
	 
			:root {
				--breadcrumb-theme-1: #a3ce54;
				--breadcrumb-theme-2: #fff;
				--breadcrumb-theme-3: #a3ce54;
				--breadcrumb-theme-4: #a3ce54;
			}
			.breadcrumb-first {
				text-align: center;
				display: inline-block;
				box-shadow: 0 2px 5px rgba(0,0,0,0.25);
				overflow: hidden;
				border-radius: 5px;
				counter-reset: flag;
				margin-bottom: 5px;
			}
			.breadcrumb-first .breadcrumb__step {
				text-decoration: none;
				outline: none;
				display: block;
				float: left;
				font-size: 12px;
				line-height: 36px;
				padding: 0 10px 0 30px;
				position: relative;
				background: var(--breadcrumb-theme-2);
				color: var(--breadcrumb-theme-1);
				transition: background 0.5s;
			}
			.breadcrumb-first .breadcrumb__step:first-child {
				padding-left: 10px;
				border-radius: 5px 0 0 5px;
			}
			.breadcrumb-first .breadcrumb__step:first-child::before {
				left: 14px;
			}
			.breadcrumb-first .breadcrumb__step:last-child {
				border-radius: 0 5px 5px 0;
				padding-right: 20px;
			}
			.breadcrumb-first .breadcrumb__step:last-child::after {
				content: none;
			}
			.breadcrumb-first .breadcrumb__step::before {
				/*content: counter(flag);*/
				/*counter-increment: flag;*/
				border-radius: 100%;
				width: 20px;
				height: 20px;
				line-height: 20px;
				margin: 8px 0;
				position: absolute;
				top: 0;
				left: 30px;
				font-weight: bold;
				background: var(--breadcrumb-theme-2);
				box-shadow: 0 0 0 1px var(--breadcrumb-theme-1);
			}
			.breadcrumb-first .breadcrumb__step::after {
				content: '';
				position: absolute;
				top: 0;
				right: -18px;
				width: 36px;
				height: 36px;
				transform: scale(0.707) rotate(45deg);
				z-index: 1;
				border-radius: 0 5px 0 50px;
				background: var(--breadcrumb-theme-2);
				transition: background 0.5s;
				box-shadow: 2px -2px 0 2px var(--breadcrumb-theme-4);
			}
			.breadcrumb-first .breadcrumb__step:first-child {
				color: var(--breadcrumb-theme-2);
				background: var(--breadcrumb-theme-1);
			}
			.breadcrumb-first .breadcrumb__step:first-child::before {
				color: var(--breadcrumb-theme-1);
			}
			.breadcrumb-first .breadcrumb__step:first-child::after {
				background: var(--breadcrumb-theme-1);
			}
			.breadcrumb-first .breadcrumb__step:hover {
				color: var(--breadcrumb-theme-2);
				background: var(--breadcrumb-theme-3);
			}
			.breadcrumb-first .breadcrumb__step:hover::before {
				color: var(--breadcrumb-theme-1);
			}
			.breadcrumb-first .breadcrumb__step:hover::after {
				color: var(--breadcrumb-theme-1);
				background: var(--breadcrumb-theme-3);
			}
    </style>
@endsection