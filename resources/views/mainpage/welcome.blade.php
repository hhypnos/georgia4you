@extends('layouts.master')
@section('content')
<main>
    <br/>
    @foreach($sections as $section)
        <section id="{{strtolower(str_replace(' ','',$section->section_title))}}" class="{{$section->section_styles['section_style']}}">

            <div class="content-title" style="background: url({{URL::to('assets/images/Brush.png')}}) no-repeat 0px 0px;">
                <h3 style="height: 0">{{translate($section->subtitle_up,session('languageID'))}}</h3>
                <h2>{{translate($section->section_title,session('languageID'))}}</h2>
                <h3 style="padding-bottom: 33px;font-size:13px;"> {!! translate($section->subtitle_down,session('languageID')) !!} </h3>
                {{--slider--}}
                {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
                {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.js"></script>--}}
                {{--<style>--}}

                    {{--/****************/--}}
                    {{--/*	 BX-SLIDER 	*/--}}
                    {{--/****************/--}}
                    {{--.client {--}}
                        {{--padding:4em 0em;--}}
                        {{--background-color: #eee;--}}

                    {{--}--}}
                    {{--.client .section-title {--}}
                        {{--margin-bottom: 6em;--}}
                    {{--}--}}
                    {{--.bx-controls {--}}
                        {{--position: relative;--}}
                    {{--}--}}
                    {{--.bx-wrapper .bx-pager {--}}
                        {{--text-align: center;--}}
                        {{--padding-top: 30px;--}}
                    {{--}--}}
                    {{--.bx-wrapper .bx-pager .bx-pager-item, .bx-wrapper .bx-controls-auto .bx-controls-auto-item {--}}
                        {{--display: inline-block;--}}
                        {{--*zoom: 1;--}}
                        {{--*display: inline;--}}
                        {{--display: none;--}}

                    {{--}--}}
                    {{--.bx-wrapper .bx-pager.bx-default-pager a {--}}
                        {{--background: #666;--}}
                        {{--text-indent: -9999px;--}}
                        {{--display: block;--}}
                        {{--width: 10px;--}}
                        {{--height: 10px;--}}
                        {{--margin: 0 5px;--}}
                        {{--outline: 0;--}}
                        {{---moz-border-radius: 5px;--}}
                        {{---webkit-border-radius: 5px;--}}
                        {{--border-radius: 5px;--}}
                    {{--}--}}


                {{--</style>--}}
                {{--<div class="carousel-client">--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo1.png"></div>--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo2.png"></div>--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo3.png"></div>--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo1.png"></div>--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo2.png"></div>--}}
                    {{--<div class="slide"><img src="https://www.grandvincent-marion.fr/_codepen/slider-logo3.png"></div>--}}
                {{--</div>--}}

                {{--<script>--}}
                    {{--/**********************/--}}
                    {{--/*	Client carousel   */--}}
                    {{--/**********************/--}}
                    {{--$('.carousel-client').bxSlider({--}}
                        {{--auto: true,--}}
                        {{--slideWidth: 234,--}}
                        {{--minSlides: 2,--}}
                        {{--maxSlides: 5,--}}
                        {{--controls: false--}}
                    {{--});--}}
                {{--</script>--}}
                {{--end slider--}}
            </div>
            <div class="content-content">
                <div class="grida">
                    @if($section->section_styles['slick'] == 1)
                        @include('layouts.slickversion')
                    @else
                        @include('layouts.withoutslickversion')
                    @endif
                </div>
            </div>

        </section>
    @endforeach
	
</main>

@endsection