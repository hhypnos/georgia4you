@extends('layouts.master')
@section('content')

<main>
    <br/>
    @foreach($sections as $section)
        <section id="{{strtolower(str_replace(' ','',$section->section_title))}}" class="what-to-do">

            <div class="content-title" style="background: url({{URL::to('assets/images/Brush.png')}}) no-repeat 0px 0px;">
                <h5 style="height: 0">{{translate($section->subtitle_up,session('languageID'))}}</h5>
                <h1 id="{{strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $section->section_title))}}">{{$section->section_title}}</h1>
                <h5 style="padding-bottom: 33px;font-size:13px;"> {!! translate($section->subtitle_down,session('languageID')) !!} </h5>
               
            </div>
            <div class="content-content">
                <div class="grida">
					@include('layouts.mobileview')
                </div>
            </div>

        </section>
    @endforeach
	
</main>
<script>
	let expandSection = (element) =>{
		let id = $(element).data("sectionid");
		$("#section-"+id).toggleClass("expanded-section").fadeToggle();
		$(element).removeClass("floated-section-btn");
		
		$('html, body').animate({
			scrollTop: $("#"+$(element).data("section-name")).offset().top - 150
		}, 100);
		
		if($(element).find(".fas").hasClass("fa-arrow-down")){
			$(element).find(".fas").removeClass("fa-arrow-down");
			$(element).find(".fas").addClass("fa-arrow-up")			
		}else{
			$(element).find(".fas").removeClass("fa-arrow-up");
			$(element).find(".fas").addClass("fa-arrow-down")			
		}
	};
	$(document).ready(function() {
		var windowHeight = $(window).height(),
		gridTop = windowHeight * .3,
		gridBottom = windowHeight * .6;
		
		$(window).on('scroll', function() {
			$('.expanded-section').each(function() {
				var thisTop = $(this).offset().top - $(window).scrollTop();
				var btnID = $(this).data("btnid");
				
				if (thisTop <= gridTop && (thisTop + $(this).height()) >= gridBottom) {
					$("#"+btnID).addClass("floated-section-btn");
				} else {
					$("#"+btnID).removeClass("floated-section-btn");
				}
			});
		});
	});
</script>
@endsection

