<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * {{SESSIONS}}
*/
Route::get('language/{any}', 'MainController@language')->name('language');
Route::get('currency/{any}', 'MainController@currency')->name('currency');
/*
 * {{DASHBOARD}}
*/
//authentificated
//Route::group(['middleware' => ['auth']], function () {
Route::get('apanel','AccountController@index');
//section content
Route::get('apanel/section','AccountController@section_content');
Route::post('apanel/sectionapplystyle','AccountController@section_apply_style');
//
Route::get('apanel/sectionarticle','AccountController@article_content');
Route::post('apanel/deletepostfromsection','AccountController@delete_post_from_section');
Route::post('apanel/deletearticlefrompost','AccountController@delete_article_from_post');
Route::get('apanel/drafts','AccountController@drafts');
Route::get('apanel/preview/{slug}','AccountController@preview_draft');

Route::get('apanel/translates/{post_id}/{langID}','AccountController@translates')->name('translated_post');
Route::post('apanel/translates/updatetranslate/{post_id}/{langID}','AccountController@update_translates')->name('update_translates');

Route::get('apanel/translatetext/{text_id}/{langID}','AccountController@text_translates')->name('translated_text');
Route::post('apanel/translates/updatetexttranslate/{text_id}/{langID}','AccountController@update_text_translates')->name('update_text_translates');

Route::get('apanel/post/{slug}','AccountController@post');
Route::post('apanel/changeorder','AccountController@changeorder');
Route::post('apanel/changeorderarticle','AccountController@changeorder_article');

/*
 * Inner Post slider
*/
Route::post('apanel/post/uploadsliderimage','AccountController@upload_slider_image');
Route::post('apanel/post/ordersliderimage','AccountController@order_slider_image');
Route::post('apanel/post/deletesliderimage','AccountController@delete_slider_image');

/*
 * Post's IMAGE manipulation
*/

Route::post('apanel/post/changeimageorder','AccountController@change_image_order');
Route::post('apanel/post/makedefaultimage','AccountController@make_default_image');
Route::post('apanel/post/deleteimage','AccountController@delete_image');
Route::post('apanel/post/uploadimage','AccountController@upload_image');
/*
 * Post Manipulation
 */
Route::get('apanel/newpost','AccountController@new_post');//insert post
Route::post('apanel/post/insertpost','AccountController@insert_post');//insert post
Route::post('apanel/post/insertposttext','AccountController@insert_post_text');//insert post text
Route::post('apanel/post/deleteposttext','AccountController@delete_post_text');//remove post text
Route::post('apanel/post/deleteposttextimage','AccountController@delete_post_text_image');//remove post text image
Route::post('apanel/post/updateposttext','AccountController@update_post_text');//update post text
Route::post('apanel/publishpost','AccountController@publishpost');//from draft to production
Route::post('apanel/publisharticle','AccountController@publish_article');//from draft to production
Route::post('apanel/post/deletepost','AccountController@delete_post');
Route::post('apanel/post/updatepost','AccountController@update_post');
Route::post('apanel/deletepostfromdrafts','AccountController@deletepostfromdrafts');
/*
 * Cover IMAGE manipulation
 */
Route::get('apanel/coverimages','AccountController@cover_images');
Route::post('apanel/coverimages/uploadcoverimages','AccountController@upload_cover_images');
Route::post('apanel/coverimages/deletecoverimages','AccountController@delete_cover_images');
//logout
Route::get('logout', 'AccountController@logout')->name('logout');
//});

/*
 * Main Routes
 */
Route::get('/', 'MainController@index');
Route::get('search/{q}', function($q=""){

    $elastic = new \App\Elastic();
//    foreach (\App\Post::where('id','<=',6)->get() as $post) {
//        $elastic->storeSingleData('tr','trs',[
//                "posts_id"          => $post->posts_id,
//                "title"             => $post->title,
//        ]);
//    }
    return $search = $elastic->searchData(300, 'posts', 'post', ['posts_id'], ['match_all' => new \stdClass()]);
//    dd($elastic->getClient());




});
Route::get('{section_slug}/{slug}', 'MainController@post')->name('post');
Route::get('{section_slug}/{slug}/{article_slug}', 'MainController@post')->name('post');
Route::post('sendfeedback','MainController@send_feedback')->name('send_feedback');