//////////////////////////////////////////////////////
// SLICK SLIDER ////////////////

$('.cards').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: false,
      centerMode: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2,
      centerMode: true
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2,
      centerMode: true,
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
  ]
});
$(".slick-prev").html('<div class="slick-btn-prev"><div class="slick-btn-prev-inner"><i class="far fa-arrow-alt-circle-left"></i><div></div>');
$(".slick-next").html('<div class="slick-btn-next"><div class="slick-btn-next-inner"><i class="far fa-arrow-alt-circle-right"></i></i><div></div>');
// SLICK SLIDER ////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
// TOP BAR SMALL LOGO LABEL ON HOVER ////////////////
let dynamicTitle = () => {
  $(".alt-service-logos-single-logo-containter").on("mouseover", function () {
    if (!$(this).hasClass("alt-service-logos-single-logo-containter-active")) {
      $(".logo-label-active").hide();
    } else {
      $(".logo-label-active").show();
    }
  });
  $(".alt-service-logos-single-logo-containter").on("mouseout", function () {
    $(".logo-label-active").show();
  });
};
dynamicTitle();
// TOP BAR SMALL LOGO LABEL ON HOVER ////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
// MOBILE MENU BTN ACTIONS //////////////////////////
  function openMobileMenu(element){
    $(".mobile-menu-body").css("width", "80%");
  }
  function closeMobileMenu(element){
    $(".mobile-menu-body").css("width", "0");
  }
// MOBILE MENU BTN ACTIONS //////////////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
// MOBILE MENU BTN ACTIONS //////////////////////////
firstScroll = true;
let mainLink = window.location.href;
let substringed = mainLink.substring(0, 24);
if(window.location.href == "http://127.0.0.1/georgia4you/public/" || window.location.href == "http://81.16.248.238:8081/georgia4you/" || window.location.href == "https://georgia4you.ge/" || substringed == "https://georgia4you.ge/#")
{
  $(window).scroll(function (event) {
    var scroll = parseInt($(window).scrollTop());

    if (scroll >= 330) {
      if (firstScroll) {
        $("header").clone().addClass("alternative-header").prependTo("body");
        dynamicTitle();
        $(".logo-image").hide();
        $(".logo-upper-image").show();
        $(".logo").css({'margin':'-17px auto 30px auto'});
      }
      firstScroll = false;
    } else {
      firstScroll = true;
      $(".alternative-header").remove();
      $(".logo").css({'margin':'30px auto 30px auto'});
      if(scroll <= 239) {
        $(".logo-image").show();
        $(".logo-upper-image").hide();
      }
    }
  });
}else{
  if($("header").hasClass("alternative-header")){
    $(".logo-image").hide();
    $(".logo-upper-image").show();
    $(".logo").css({'margin':'-17px auto 30px auto'});
  }
}
// MOBILE MENU BTN ACTIONS //////////////////////////
//////////////////////////////////////////////////////
// ===== Scroll to Top ====
$(window).scroll(function() {
  if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
    $('#return-to-top').fadeIn(200);    // Fade in the arrow
  } else {
    $('#return-to-top').fadeOut(200);   // Else fade out the arrow
  }
});
$('#return-to-top').click(function() {      // When arrow is clicked
  $('body,html').animate({
    scrollTop : 0                       // Scroll to top of body
  }, 500);
});

//==============preloader ============
// $('body,html').animate({
//   scrollTop : 0                       // Scroll to top of body
// }, 500);
$(window).on('load',function() {
  $("#page-preloader").fadeOut('slow',function(){
    $('body').removeClass('loading');
  });
let frameScroll = false;
  $(window).scroll(function() {
  let frameClass = $('.frame-class');
    if(frameClass[0] && frameClass.position().top > ($(window).scrollTop()+$(window).height()) && !frameScroll){
      let iFrame = document.createElement("iframe");
      iFrame.src = frameClass.data('linksrc');
      iFrame.width = frameClass.data('linkwidth');
      iFrame.height = frameClass.data('linkheight');
      frameClass.append(iFrame);
      frameScroll = true;
    }
  });

});

